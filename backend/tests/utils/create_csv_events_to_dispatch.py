from typing import List

import pandas as pd

from adapters.pandas.pandas_helpers import event_to_series
from helpers.factories.vehicle_event_factory import make_vehicle_event
from helpers.uuid import uuid4
from tests.integration.csv_test_helpers import reset_csv


def create_df_events_to_dispatch(timestamps: List[pd.Timestamp]):
    list_series = []
    for timestamp in timestamps:
        event = make_vehicle_event(timestamp=timestamp, uuid=uuid4())
        list_series.append(event_to_series(event))

    df = pd.DataFrame(list_series)
    return df


def create_csv_events_to_dispatch(path: str, timestamps: List[pd.Timestamp]):
    reset_csv(path)
    df = create_df_events_to_dispatch(timestamps)
    df.to_csv(path)
    return df
