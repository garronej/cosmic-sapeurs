from typing import Any, List

from domain.core.domain_event_bus import DomainTopic
from domain.core.event_bus import InMemoryEventBus


def spy_on_topic(event_bus: InMemoryEventBus, topic: DomainTopic) -> List[Any]:
    published_events = []

    async def spy(e):
        published_events.append(e)

    event_bus.subscribe(topic, spy)
    return published_events
