from domain.core.domain_event_bus import DomainEventBus
from domain.couv_ops.ports.counts_repository import InMemoryCountsRepository
from domain.couv_ops.ports.vehicle_events_repository import (
    InMemoryVehicleEventRepository,
)
from domain.couv_ops.use_cases.update_couvops import UpdateCouvops
from helpers.uuid import CustomUuid


def prepare_update_couvops(vehicle_event_repo: InMemoryVehicleEventRepository = None):
    vehicle_event_repo = vehicle_event_repo or InMemoryVehicleEventRepository()
    counts_repo = InMemoryCountsRepository()
    domain_event_bus = DomainEventBus()
    custom_uuid = CustomUuid()
    update_couvops = UpdateCouvops(
        vehicle_event_repo=vehicle_event_repo,
        counts_repo=counts_repo,
        domain_event_bus=domain_event_bus,
        uuid=custom_uuid,
    )
    return (
        vehicle_event_repo,
        counts_repo,
        update_couvops,
        domain_event_bus,
        custom_uuid,
    )
