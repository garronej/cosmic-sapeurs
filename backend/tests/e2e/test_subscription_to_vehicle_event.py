import asyncio

import pandas as pd

from domain.core.domain_event_bus import DomainEventBus
from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    CouvOpsUpdatedEventData,
    VehicleAvailability,
)
from domain.couv_ops.entities.event_entities import CouvOpsUpdatedEventEntity
from domain.couv_ops.events.events import VehicleEvent
from helpers.date import DateStr
from helpers.factories.vehicle_event_factory import (
    make_vehicle_event,
    make_vehicle_event_data,
)
from helpers.uuid import uuid4
from tests.utils.prepare_update_couvops import prepare_update_couvops

# --- Todo: Record vehicle event elsewhere
# -----------------------------------------
# def test_inmemory_subscription_to_vehicle_event():
#     vehicle_event_repo, _, update_couvops, _, _ = prepare_update_couvops()
#     event_bus = DomainEventBus()
#     event_bus.subscribe(topic="vehicle_changed_status", callback=update_couvops.execute)
#     incoming_vehicle_event_data = make_vehicle_event_data()
#     incoming_vehicle_event = make_vehicle_event(
#         data=incoming_vehicle_event_data, uuid="my uuid"
#     )
#     expected_vehicle_event_entity = make_vehicle_event_entity(
#         data=incoming_vehicle_event_data, uuid="my uuid"
#     )
#     # publish event
#     asyncio.run(event_bus.publish(event=incoming_vehicle_event))
#     #  check that repo contains the data just published
#     assert vehicle_event_repo.vehicle_events == [expected_vehicle_event_entity]


def test_event_available_count_is_updated():
    _, counts_repo, update_couvops, _, _ = prepare_update_couvops()
    event_bus = DomainEventBus()
    event_bus.subscribe(topic="vehicle_changed_status", callback=update_couvops.execute)

    def on_event_assert_count(
        event: VehicleEvent, expected_count: CouvOpsUpdatedEventEntity
    ):
        asyncio.run(event_bus.publish(event))
        assert counts_repo.last_count.timestamp == expected_count.timestamp
        assert counts_repo.last_count.data == expected_count.data

    # First event
    victim_rescue_1_became_available_event = make_vehicle_event(
        role="victim_rescue",
        raw_vehicle_id=1,
        status="misc_available",
        timestamp=pd.Timestamp("2020-10-01T12:00"),
    )

    on_event_assert_count(
        victim_rescue_1_became_available_event,
        CouvOpsUpdatedEventEntity(
            timestamp=pd.Timestamp("2020-10-01T12:00"),
            uuid=uuid4(),
            data=CouvOpsUpdatedEventData(
                role="victim_rescue", counts=VehicleAvailability(available=1)
            ),
        ),
    )

    # Second event : vehicle #2 as pump became available
    pump_2_became_available_event = make_vehicle_event(
        role="pump",
        raw_vehicle_id=2,
        status="misc_available",
        timestamp=DateStr("2020-10-01T13:00"),
    )
    on_event_assert_count(
        pump_2_became_available_event,
        CouvOpsUpdatedEventEntity(
            timestamp=DateStr("2020-10-01T13:00"),
            uuid=uuid4(),
            data=CouvOpsUpdatedEventData(
                role="pump", counts=VehicleAvailability(available=1)
            ),
        ),
    )

    # Third event : vehicle #1 as victim_rescue was selected
    victim_rescue_1_was_selected_event = make_vehicle_event(
        role="victim_rescue",
        raw_vehicle_id=1,
        status="selected",
        timestamp=DateStr("2020-10-01T13:03"),
    )
    on_event_assert_count(
        victim_rescue_1_was_selected_event,
        CouvOpsUpdatedEventEntity(
            timestamp=DateStr("2020-10-01T13:03"),
            uuid=uuid4(),
            data=CouvOpsUpdatedEventData(
                role="victim_rescue", counts=VehicleAvailability(in_service=1)
            ),
        ),
    )

    # Fourth event : vehicle #3 as other is new and in sport
    other_3_is_new_and_in_sport_event = make_vehicle_event(
        role="other",
        raw_vehicle_id=3,
        status="recoverable_within_15_minutes",
        timestamp=DateStr("2020-10-01T14:00"),
    )
    on_event_assert_count(
        other_3_is_new_and_in_sport_event,
        CouvOpsUpdatedEventEntity(
            timestamp=DateStr("2020-10-01T14:00"),
            uuid=uuid4(),
            data=CouvOpsUpdatedEventData(
                role="other", counts=VehicleAvailability(recoverable=1)
            ),
        ),
    )

    # Fifth event :
    victim_rescue_1_was_selected_event = make_vehicle_event(
        role="victim_rescue",
        raw_vehicle_id=1,
        status="arrived_at_home",
        timestamp=DateStr("2020-10-01T20:09"),
    )
    on_event_assert_count(
        victim_rescue_1_was_selected_event,
        CouvOpsUpdatedEventEntity(
            timestamp=DateStr("2020-10-01T20:09"),
            uuid=uuid4(),
            data=CouvOpsUpdatedEventData(
                role="victim_rescue", counts=VehicleAvailability(available=1)
            ),
        ),
    )
