import os
from dataclasses import asdict

import pandas as pd
import pytest
from pandas._libs.tslibs import Timestamp

from adapters.pandas.pandas_helpers import event_entity_to_series
from adapters.pandas.pandas_vehicle_events_repository import (
    PandasVehicleEventsRepository,
)
from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    VehicleAvailability,
)
from domain.couv_ops.data_transfert_objects.vehicle_event_data import RawVehicleId
from domain.couv_ops.ports.vehicle_events_repository import (
    AlreadyExistingVehicleEventUuid,
    CountsByRole,
    vehicle_role_options,
)
from helpers.factories.vehicle_event_factory import (
    make_vehicle_event_data,
    make_vehicle_event_entity,
)
from tests.integration.csv_test_helpers import reset_csv

csv_path = "tests/integration/temp_data/vehicle_event_entities.csv"


def test_creates_csv_if_provided_csv_path_does_not_exists():
    # remove csv
    intitialy_no_file_csv_path = (
        "tests/integration/temp_data/intitialy_no_file_csv_path.csv"
    )
    if os.path.exists(intitialy_no_file_csv_path):
        os.remove(intitialy_no_file_csv_path)
    _ = PandasVehicleEventsRepository(csv_path=intitialy_no_file_csv_path)
    df = pd.read_csv(intitialy_no_file_csv_path)
    assert df.empty


def test_can_add_to_pandas_vehicle_events_repository():
    reset_csv(csv_path)
    pandas_vehicle_events_repository = PandasVehicleEventsRepository(csv_path=csv_path)
    vehicle_event_entity = make_vehicle_event_entity()
    pandas_vehicle_events_repository.add(vehicle_event_entity)
    df = pd.read_csv(csv_path)
    last_row = df.iloc[-1]
    assert not last_row.empty
    assert last_row.uuid == vehicle_event_entity.uuid
    assert last_row.timestamp == vehicle_event_entity.timestamp
    last_row_values = set(last_row.values)
    vehicle_event_entity_data_values = set(asdict(vehicle_event_entity.data).values())
    assert vehicle_event_entity_data_values.issubset(last_row_values)


def test_cannot_add_to_csv_if_already_exists():
    reset_csv(csv_path)
    pandas_vehicle_events_repository = PandasVehicleEventsRepository(csv_path=csv_path)
    vehicle_event_entity = make_vehicle_event_entity()

    with pytest.raises(AlreadyExistingVehicleEventUuid):
        # add the same event two times
        pandas_vehicle_events_repository.add(vehicle_event_entity)
        pandas_vehicle_events_repository.add(vehicle_event_entity)


def make_vehicle_event_series(timestamp: Timestamp = None, uuid: str = None, **kwargs):
    event_entity = make_vehicle_event_entity(timestamp=timestamp, uuid=uuid, **kwargs)
    return event_entity_to_series(event_entity)


def test_resync_if_df_is_empty():
    reset_csv(csv_path)
    pd.DataFrame().to_csv(csv_path)
    pandas_vehicle_events_repository = PandasVehicleEventsRepository(csv_path)

    assert (
        pandas_vehicle_events_repository.resync_current_availability_counts_by_role()
        == {role: VehicleAvailability() for role in vehicle_role_options}
    )
    assert (
        pandas_vehicle_events_repository.resync_current_availability_by_vehicle_raw_id()
        == {}
    )


def test_resync():
    reset_csv(csv_path)
    events_kwargs = [
        {
            "raw_vehicle_id": 1,
            "status": "selected",
            "role": "victim_rescue",
        },
        {
            "raw_vehicle_id": 2,
            "status": "arrived_on_intervention",
            "role": "victim_rescue",
        },
        {
            "raw_vehicle_id": 2,
            "status": "arrived_at_home",
            "role": "victim_rescue",
        },
        {
            "raw_vehicle_id": 3,
            "status": "broken",
            "role": "pump",
        },
    ]
    df = pd.DataFrame(
        [
            make_vehicle_event_series(
                timestamp=pd.Timestamp(f"2020-01-01T1{k}"), **event_kwargs
            )
            for k, event_kwargs in enumerate(events_kwargs)
        ]
    )
    df.to_csv(csv_path, index=False)

    pandas_vehicle_events_repository = PandasVehicleEventsRepository(csv_path=csv_path)

    actual_count_by_role = (
        pandas_vehicle_events_repository.resync_current_availability_counts_by_role()
    )

    expected_count_by_role: CountsByRole = {
        "victim_rescue": VehicleAvailability(available=1, in_service=1),
        "pump": VehicleAvailability(unavailable=1),
    }
    assert actual_count_by_role == expected_count_by_role

    actual_availability_by_vehicle_raw_id = (
        pandas_vehicle_events_repository.resync_current_availability_by_vehicle_raw_id()
    )

    expected_availability_by_vehicle_raw_id = {
        RawVehicleId(1): "in_service",
        RawVehicleId(2): "available",
        RawVehicleId(3): "unavailable",
    }
    assert (
        actual_availability_by_vehicle_raw_id == expected_availability_by_vehicle_raw_id
    )
