import asyncio
from datetime import datetime
from uuid import uuid4

import pandas as pd
import pytest
from pandas.api.types import is_datetime64_any_dtype as is_datetime

from adapters.pandas.pandas_helpers import event_to_series
from adapters.scheduled.scheduled_event_bus import DataFrameError, ReplayedEventBus
from helpers.clock import CustomClock
from helpers.date import DateStr
from helpers.factories.vehicle_event_factory import make_vehicle_event
from tests.integration.csv_test_helpers import reset_csv
from tests.utils.create_csv_events_to_dispatch import create_csv_events_to_dispatch
from tests.utils.spy_on_topic import spy_on_topic

csv_path = "tests/integration/temp_data/events_to_dispatch.csv"


def test_fails_if_csv_not_found():
    clock = CustomClock()
    with pytest.raises(DataFrameError) as e:
        ReplayedEventBus(clock, csv_path="notfound/file.csv")
    assert str(e.value) == "Csv_path or DF should be provided"

    with pytest.raises(DataFrameError) as e2:
        ReplayedEventBus(clock)
    assert str(e2.value) == "Csv_path or DF should be provided"


def test_fails_if_csv_format_is_incorrect():
    clock = CustomClock()
    timestamps = [
        pd.Timestamp("2020-10-01T12:01"),
        pd.Timestamp("2020-10-01T12:01:03"),
        pd.Timestamp("2020-10-01T12:00"),
    ]
    df = create_csv_events_to_dispatch(csv_path, timestamps)
    missing_col_df = df.drop("timestamp", axis=1)
    with pytest.raises(DataFrameError) as e3:
        ReplayedEventBus(clock, df=missing_col_df)
    assert str(e3.value) == "Some columns are missing : {'timestamp'}"

    timestamp_as_str_df = df.copy()
    timestamp_as_str_df.timestamp = timestamp_as_str_df.timestamp
    event_bus = ReplayedEventBus(clock, df=timestamp_as_str_df.copy())
    assert is_datetime(event_bus.df.timestamp)
    assert event_bus.df.timestamp.is_monotonic

    timestamp_with_wrong_str_df = timestamp_as_str_df
    timestamp_with_wrong_str_df.loc[0, "timestamp"] = "wrong"
    with pytest.raises(DataFrameError) as e4:
        ReplayedEventBus(clock, df=timestamp_with_wrong_str_df)
    assert str(e4.value) == "Timestamp cannot be converted to datetime"


def prepare_event_bus_and_spy(
    resync: bool, time_step: float, speed: float, clock: CustomClock = None
):
    timestamps = [
        pd.Timestamp("2020-10-01T12:01"),
        pd.Timestamp("2020-10-01T12:01:03"),
        pd.Timestamp("2020-10-01T12:00"),
    ]

    df = create_csv_events_to_dispatch(csv_path, timestamps)
    clock = clock or CustomClock()

    event_bus = ReplayedEventBus(
        clock, df=df, resync=resync, time_step=time_step, speed=speed
    )
    published_events = spy_on_topic(event_bus, "vehicle_changed_status")

    event_bus.start()
    return event_bus, published_events, clock


async def _async_test_one_step(
    event_bus, published_events, expected_dispatched_events_timestamps
):
    # dispatch first event
    clock = event_bus.clock
    awaken_event = asyncio.Event()
    looping_task = asyncio.create_task(event_bus.async_loop())
    clock.set_awaken_event(awaken_event)
    await clock.wake_up()
    await looping_task
    assert [
        event.timestamp for event in published_events
    ] == expected_dispatched_events_timestamps


def test_provided_event_get_dispatched_correctly():
    speed = 1
    time_step = 1
    resync = False

    event_bus, published_events, clock = prepare_event_bus_and_spy(
        resync, time_step, speed
    )

    expected_dispatched_events_timestamps = [pd.Timestamp("2020-10-01 12:00:00")]
    clock.add_seconds(1)
    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            expected_dispatched_events_timestamps,
        )
    )

    # Adding 5 seconds and call next.
    clock.add_seconds(5)
    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            expected_dispatched_events_timestamps,
        )
    )

    # Adding 60 seconds and call next
    clock.add_seconds(60)
    expected_dispatched_events_timestamps = [
        pd.Timestamp("2020-10-01 12:00:00"),
        pd.Timestamp("2020-10-01T12:01"),
        pd.Timestamp("2020-10-01T12:01:03"),
    ]
    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            expected_dispatched_events_timestamps,
        )
    )


def test_streaming_speed():
    speed = 60
    time_step = 1
    resync = False

    event_bus, published_events, clock = prepare_event_bus_and_spy(
        resync, time_step, speed
    )

    clock.add_seconds(1)  # equivalent to 60 seconds

    expected_dispatched_events_timestamps = [
        pd.Timestamp("2020-10-01 12:00:00"),
        pd.Timestamp("2020-10-01T12:01"),
    ]
    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            expected_dispatched_events_timestamps,
        )
    )


def test_resync():
    speed = 1
    time_step = 1
    resync = True

    test_start_time = DateStr("2020-10-02T15:00:00")

    clock = CustomClock(test_start_time)
    event_bus, published_events, clock = prepare_event_bus_and_spy(
        resync, time_step, speed, clock=clock
    )
    clock.add_seconds(1)

    expected_dispatched_events_timestamps = [pd.Timestamp(test_start_time)]
    asyncio.run(
        _async_test_one_step(
            event_bus,
            published_events,
            expected_dispatched_events_timestamps,
        )
    )
