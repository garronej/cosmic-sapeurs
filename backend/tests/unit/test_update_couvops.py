import asyncio

import pandas as pd
import pytest

from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    CouvOpsUpdatedEventData,
    VehicleAvailability,
)
from domain.couv_ops.entities.event_entities import CouvOpsUpdatedEventEntity
from domain.couv_ops.ports.vehicle_events_repository import (
    InMemoryVehicleEventRepository,
)
from domain.couv_ops.use_cases.update_couvops import UnknownRoleError
from helpers.date import DateStr
from helpers.factories.vehicle_event_factory import (
    make_vehicle_event,
    make_vehicle_event_data,
    make_vehicle_event_entity,
)
from tests.utils.prepare_update_couvops import prepare_update_couvops
from tests.utils.spy_on_topic import spy_on_topic


def test_vehicle_event_gets_stored_and_couv_ops_updated_event_gets_published():
    (
        vehicle_event_repo,
        counts_repo,
        update_couvops,
        domain_event_bus,
        custom_uuid,
    ) = prepare_update_couvops()

    published_events = spy_on_topic(domain_event_bus, "vehicleAvailabilityChanged")
    timestamp = DateStr("2020-10-01T12:00")

    incoming_vehicle_event = make_vehicle_event(
        uuid="vehicle_uuid",
        status="selected",
        role="victim_rescue",
        timestamp=timestamp,
    )

    couvops_uuid = "couvops_uuid"

    expected_couvops_updated_event_entity = CouvOpsUpdatedEventEntity(
        timestamp=timestamp,
        uuid=couvops_uuid,
        data=CouvOpsUpdatedEventData(
            role="victim_rescue",
            counts=VehicleAvailability(in_service=1),
        ),
    )

    custom_uuid.set_next_uuid(couvops_uuid)

    asyncio.run(update_couvops.execute(event=incoming_vehicle_event))

    assert counts_repo.counts == [expected_couvops_updated_event_entity]
    assert len(published_events) == 1
    assert published_events[0].uuid == couvops_uuid
    assert published_events[0].data.counts == VehicleAvailability(in_service=1)


def test_vehicle_event_gets_stored_and_couv_ops_updated_event_gets_published_when_repo_has_data():
    old_event_entity = make_vehicle_event_entity(
        raw_vehicle_id=1, status="departed_to_intervention", role="victim_rescue"
    )
    vehicle_event_repo = InMemoryVehicleEventRepository()
    vehicle_event_repo.vehicle_events = [old_event_entity]
    (
        vehicle_event_repo,
        counts_repo,
        update_couvops,
        _,
        custom_uuid,
    ) = prepare_update_couvops(vehicle_event_repo=vehicle_event_repo)
    timestamp = DateStr("2020-10-01T12:00")

    incoming_vehicle_event = make_vehicle_event(
        uuid="vehicle_uuid",
        status="selected",
        role="victim_rescue",
        raw_vehicle_id=2,
        timestamp=timestamp,
    )

    expected_counts = VehicleAvailability(recoverable=0, in_service=2)

    couvops_uuid = "couvops_uuid"
    expected_couvops_updated_event_entity = CouvOpsUpdatedEventEntity(
        timestamp=timestamp,
        uuid=couvops_uuid,
        data=CouvOpsUpdatedEventData(role="victim_rescue", counts=expected_counts),
    )
    custom_uuid.set_next_uuid(couvops_uuid)

    asyncio.run(update_couvops.execute(event=incoming_vehicle_event))

    assert counts_repo.counts[-1].data.counts == expected_counts
    assert counts_repo.counts == [expected_couvops_updated_event_entity]


def test_couv_ops_correctly_updated_when_availability_kind_remains_unchanged():
    (
        _,
        counts_repo,
        update_couvops,
        _,
        _,
    ) = prepare_update_couvops()
    timestamp = DateStr("2020-10-01T12:00")
    incoming_vehicle_event_1 = make_vehicle_event(
        status="selected",
        role="victim_rescue",
        raw_vehicle_id=1,
        timestamp=timestamp,
    )
    incoming_vehicle_event_2 = make_vehicle_event(
        status="arrived_at_hospital",
        role="victim_rescue",
        raw_vehicle_id=1,
        timestamp=timestamp,
    )
    expected_counts = VehicleAvailability(in_service=1)

    asyncio.run(update_couvops.execute(event=incoming_vehicle_event_1))
    asyncio.run(update_couvops.execute(event=incoming_vehicle_event_2))

    assert counts_repo.counts[-1].data.counts == expected_counts


def test_receives_event_with_unknown_role():
    _, _, update_couvops, _, _ = prepare_update_couvops()

    incoming_vehicle_event = make_vehicle_event(role="unknown_role", raw_vehicle_id=1)
    with pytest.raises(UnknownRoleError) as e:
        asyncio.run(update_couvops.execute(event=incoming_vehicle_event))
    assert str(e.value) == "Received vehicle event with unknown role : unknown_role"
