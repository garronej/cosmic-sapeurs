from time import time

import pytest

from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    VehicleAvailability,
)
from domain.couv_ops.ports.vehicle_events_repository import (
    AlreadyExistingVehicleEventUuid,
    InMemoryVehicleEventRepository,
)
from helpers.factories.vehicle_event_factory import (
    make_vehicle_event_data,
    make_vehicle_event_entity,
)


def test_can_add_to_vehicle_events_repository():
    vehicle_events_repository = InMemoryVehicleEventRepository()
    vehicle_event_entity = make_vehicle_event_entity()
    vehicle_events_repository.add(vehicle_event_entity)

    assert vehicle_events_repository.get_all() == [vehicle_event_entity]


def test_cannot_add_if_already_exists():
    vehicle_events_repository = InMemoryVehicleEventRepository()
    vehicle_event_entity = make_vehicle_event_entity()

    with pytest.raises(AlreadyExistingVehicleEventUuid):
        # add the same event two times
        vehicle_events_repository.add(vehicle_event_entity)
        vehicle_events_repository.add(vehicle_event_entity)


def test_count_by_role():
    vehicle_events_repository = InMemoryVehicleEventRepository()
    vehicle_events_repository.add(
        make_vehicle_event_entity(
            role="victim_rescue", status="arrived_at_home", raw_vehicle_id=1
        )
    )
    vehicle_events_repository.add(
        make_vehicle_event_entity(
            role="victim_rescue", status="arrived_on_intervention", raw_vehicle_id=2
        )
    )
    vehicle_events_repository.add(
        make_vehicle_event_entity(
            role="pump", status="misc_available", raw_vehicle_id=3
        )
    )

    assert vehicle_events_repository.resync_current_availability_counts_by_role() == {
        "pump": VehicleAvailability(available=1),
        "victim_rescue": VehicleAvailability(available=1, in_service=1),
        "other": VehicleAvailability(),
    }


def test_last_status_by_vehicle_raw_id():
    vehicle_events_repository = InMemoryVehicleEventRepository()
    vehicle_events_repository.add(
        make_vehicle_event_entity(raw_vehicle_id=1, status="left_hospital")
    )
    vehicle_events_repository.add(
        make_vehicle_event_entity(raw_vehicle_id=2, status="arrived_on_intervention")
    )
    vehicle_events_repository.add(
        make_vehicle_event_entity(raw_vehicle_id=1, status="arrived_at_home")
    )
    assert (
        vehicle_events_repository.resync_current_availability_by_vehicle_raw_id()
        == {
            1: "available",
            2: "in_service",
        }
    )


def test_last_available_count_by_role():
    vehicle_events_repository = InMemoryVehicleEventRepository()
    vehicle_events_repository.add(
        make_vehicle_event_entity(
            raw_vehicle_id=1, status="left_hospital", role="victim_rescue"
        )
    )
    vehicle_events_repository.add(
        make_vehicle_event_entity(
            raw_vehicle_id=2, status="arrived_on_intervention", role="pump"
        )
    )
    vehicle_events_repository.add(
        make_vehicle_event_entity(
            raw_vehicle_id=3, status="arrived_at_home", role="pump"
        )
    )
    vehicle_events_repository.add(
        make_vehicle_event_entity(
            raw_vehicle_id=1, status="arrived_at_home", role="victim_rescue"
        )
    )
    assert vehicle_events_repository.resync_current_availability_counts_by_role() == {
        "victim_rescue": VehicleAvailability(available=1),
        "pump": VehicleAvailability(available=1, in_service=1),
        "other": VehicleAvailability(),
    }


# Todo: repair this test, ie. handle omnibus
# ------------------------------------------
# def test_last_available_count_by_role_with_omnibus():
#     # vehicle 1 turned from "victim_rescue" to "pump"
#     # vehicle 4 changed for status "lacks_staff"
#     vehicle_events_repository = InMemoryVehicleEventRepository()
#     vehicle_events_repository.add(
#         make_vehicle_event_entity(
#             data=make_vehicle_event_data(
#                 raw_vehicle_id=1, status="left_hospital", role="victim_rescue"
#             )
#         )
#     )
#     vehicle_events_repository.add(
#         make_vehicle_event_entity(
#             data=make_vehicle_event_data(
#                 raw_vehicle_id=2, status="arrived_at_home", role="pump"
#             )
#         )
#     )
#     vehicle_events_repository.add(
#         make_vehicle_event_entity(
#             data=make_vehicle_event_data(
#                 raw_vehicle_id=1, status="misc_available", role="pump"
#             )
#         )
#     )
#     vehicle_events_repository.add(
#         make_vehicle_event_entity(
#             data=make_vehicle_event_data(
#                 raw_vehicle_id=4, status="lacks_staff", role="victim_rescue"
#             )
#         )
#     )
#     assert vehicle_events_repository.resync_current_availability_counts_by_role() == {
#         "victim_rescue": {
#             "available": 0,
#             "unavailable": 0,
#             "recoverable": 0,
#             "in_service": 0,
#         },
#         "pump": {
#             "available": 2,
#             "unavailable": 0,
#             "recoverable": 0,
#             "in_service": 0,
#         },
#         "other": {
#             "available": 0,
#             "unavailable": 0,
#             "recoverable": 0,
#             "in_service": 0,
#         },
#     }
