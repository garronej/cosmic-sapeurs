import asyncio

import pandas as pd

from adapters.pandas.pandas_vehicle_events_repository import (
    PandasVehicleEventsRepository,
)
from adapters.scheduled.scheduled_event_bus import ReplayedEventBus
from domain.core.domain_event_bus import DomainEventBus
from domain.couv_ops.ports.vehicle_events_repository import (
    InMemoryVehicleEventRepository,
)
from domain.couv_ops.use_cases.prepare_replayed_event_bus import PrepareReplayedEventBus
from helpers.clock import CustomClock
from helpers.date import DateStr, from_datetime, to_datetime, to_timestamp
from tests.integration.csv_test_helpers import reset_csv
from tests.utils.create_csv_events_to_dispatch import create_df_events_to_dispatch
from tests.utils.spy_on_topic import spy_on_topic


def test_prepare_replayed_event_bus():
    timestamps = [
        pd.Timestamp("2020-10-01T12:01"),
        pd.Timestamp("2020-10-01T12:02"),
        pd.Timestamp("2020-10-01T12:03"),
        pd.Timestamp("2020-10-01T12:04"),
    ]
    df = create_df_events_to_dispatch(timestamps)

    domain_event_bus = DomainEventBus()
    published_events = spy_on_topic(domain_event_bus, "replayedEventsBusBecameReady")

    clock = CustomClock()
    became_ready_date = DateStr("2020-01-01T12:00:00")
    clock.set_next_date(became_ready_date)
    event_bus = ReplayedEventBus(df=df, clock=clock)
    csv_path = "data/test_prepare_replayed_events_bus.csv"
    reset_csv(csv_path)
    vehicle_events_repo = PandasVehicleEventsRepository(csv_path)
    prepare_replayed_event_bus = PrepareReplayedEventBus(
        event_bus=event_bus,
        vehicle_events_repo=vehicle_events_repo,
        domain_event_bus=domain_event_bus,
        clock=clock,
    )

    asyncio.run(prepare_replayed_event_bus.execute(date=DateStr("2020-10-01T12:03:01")))

    assert len(vehicle_events_repo.df) == 3
    assert event_bus._last_published_timestamp == pd.Timestamp("2020-10-01T12:03:01")

    assert len(published_events) == 1
    assert published_events[0].timestamp == became_ready_date
    assert published_events[0].topic == "replayedEventsBusBecameReady"
