import os
import random
from abc import abstractmethod
from typing import Union

import pandas as pd
from pandas.api.types import is_datetime64_any_dtype as is_datetime
from typing_extensions import Literal

from adapters.pandas.pandas_helpers import event_to_series, series_to_event
from domain.core.domain_event_bus import DomainTopic
from domain.core.event_bus import InMemoryEventBus
from helpers.clock import AbstractClock
from helpers.date import DateStr, to_datetime, to_timestamp
from helpers.factories.vehicle_event_factory import (
    make_vehicle_event,
    make_vehicle_event_data,
)


class DataFrameError(Exception):
    pass


class Pause(Exception):
    pass


ExternalTopic = Literal["vehicle_changed_status"]


class ScheduledEventBus(InMemoryEventBus[ExternalTopic]):
    def __init__(self, clock: AbstractClock):
        super().__init__()
        self.clock = clock

    @property
    def _keep_looping(self):
        return True

    @abstractmethod
    def start(self):
        raise NotImplementedError

    async def _dispatch_event(self, event_series: pd.Series):
        topic: DomainTopic = event_series.topic
        if topic == "vehicle_changed_status":
            event = series_to_event(event_series)
            await self.publish(event)

    @abstractmethod
    async def _async_next(self):
        raise NotImplementedError

    async def async_loop(self):
        while self.clock.can_wake_up and self._keep_looping:
            await self._async_next()


class RandomScheduledEventBus(ScheduledEventBus):
    def __init__(
        self, clock: AbstractClock, *, min_time_step: float, max_time_step: float
    ):
        super().__init__(clock)
        self.min_time_step = min_time_step
        self.max_time_step = max_time_step

    def start(self):
        pass

    async def _async_next(self):
        await self.clock.sleep(random.uniform(self.min_time_step, self.max_time_step))
        vehicle_event_data = make_vehicle_event_data()
        vehicle_event = make_vehicle_event(
            data=vehicle_event_data, timestamp=pd.Timestamp(self.clock.get_now())
        )
        await self._dispatch_event(event_to_series(vehicle_event))


class ReplayedEventBus(ScheduledEventBus):
    def __init__(
        self,
        clock: AbstractClock,
        *,
        csv_path: str = None,
        df: pd.DataFrame = None,
        time_step: float = 1.0,
        speed: float = 1,
        resync: bool = False,
    ):
        super().__init__(clock)

        self.df: pd.DataFrame
        if df is not None:
            self.df = df.copy()  # copy df to avoid mutating the input one
        else:
            if csv_path is None or not os.path.exists(csv_path):
                raise DataFrameError("Csv_path or DF should be provided")
            self.df = pd.read_csv(csv_path, index_col=0)
        self._sanity_check()
        self.df.index = self.df.timestamp
        self.last_row_timestamp = self.df.index[-1]
        self._last_published_timestamp = self.df.timestamp[0]
        self.time_step = time_step
        self.speed = speed
        self.resync = resync

    def start(self):
        now = self.clock.get_now()
        self.offset = pd.Timestamp(now) - self._last_published_timestamp
        self._previous_now = to_datetime(now)

    async def _async_next(self):
        now = to_datetime(self.clock.get_now())
        ellapsed = now - self._previous_now
        if ellapsed.total_seconds() > self.time_step:
            print(
                f"\n --> --> Congestion ! You should set time_step greater than {ellapsed.total_seconds()} s \n"
            )
        sleep_time = max(self.time_step - ellapsed.total_seconds(), 0)
        await self.clock.sleep(sleep_time)
        next_timestamp_begins = self._last_published_timestamp
        next_timestamp_ends = next_timestamp_begins + ellapsed * self.speed
        rows_to_dispatch = self.df[next_timestamp_begins:next_timestamp_ends]
        for _, serie in rows_to_dispatch.iterrows():
            if self.resync:
                serie["timestamp"] += self.offset
            await self._dispatch_event(serie)
        self._previous_now = now
        self._last_published_timestamp = next_timestamp_ends

    @property
    def _keep_looping(self) -> bool:
        return self.last_row_timestamp > self._last_published_timestamp

    def _sanity_check(self):
        self._has_required_columns()
        self._timestamps_is_datetime()
        self._timestamps_is_monotonic()
        # todo eventually
        # self._literals_are_valid()
        # self._uuid_are_unique()

    def _has_required_columns(self) -> None:
        required_columns = {"uuid", "timestamp", "topic"}
        columns = set(self.df.columns)
        missing_columns = required_columns - columns
        if missing_columns:
            raise DataFrameError(f"Some columns are missing : {missing_columns}")

    def _timestamps_is_datetime(self):
        if not is_datetime(self.df.timestamp):
            try:
                self.df.timestamp = pd.to_datetime(self.df.timestamp)
            except Exception:
                raise DataFrameError("Timestamp cannot be converted to datetime")

    def _timestamps_is_monotonic(self):
        if not self.df.timestamp.is_monotonic:
            self.df.sort_values("timestamp", inplace=True)

    @staticmethod
    def _to_timestamp_or_default(
        date: Union[DateStr, None], default: pd.Timestamp
    ) -> pd.Timestamp:
        if date:
            return to_timestamp(date)
        return default

    def get_events_between(
        self, start_date: DateStr = None, end_date: DateStr = None
    ) -> pd.DataFrame:
        start_date = self._to_timestamp_or_default(start_date, self.df.timestamp.min())
        end_date = self._to_timestamp_or_default(end_date, self.df.timestamp.max())
        rows = self.df.set_index("timestamp")[start_date:end_date].reset_index()
        return rows

    def set_last_published_timestamp(self, date: DateStr):
        self._last_published_timestamp = to_timestamp(date)
