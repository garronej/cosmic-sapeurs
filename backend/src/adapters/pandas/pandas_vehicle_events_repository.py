import csv
import os
from dataclasses import Field, asdict, fields
from typing import Dict, List, Union

import pandas as pd

from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    VehicleAvailability,
)
from domain.couv_ops.data_transfert_objects.vehicle_event_data import (
    VehicleEventData,
    vehicle_role_options,
    vehicle_status_to_availability_kind,
)
from domain.couv_ops.entities.event_entities import VehicleEventEntity
from domain.couv_ops.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
    AvailabilityKindByVehicleId,
    CountsByRole,
)
from helpers.date import from_timestamp


class PandasVehicleEventsRepository(AbstractVehicleEventsRepository):
    def __init__(self, csv_path: str = "data/vehicle_event_entities.csv"):
        self.csv_path = csv_path
        csv_path_exists = os.path.isfile(csv_path)
        if csv_path_exists:
            self.df = pd.read_csv(csv_path, index_col=0)
        else:
            self.init_empty_df()

    def init_empty_df(self):
        event_fields = [
            field for field in fields(VehicleEventEntity) if field.name != "data"
        ]
        data_fields = list(fields(VehicleEventData))
        columns_fields = event_fields + data_fields

        os.makedirs(os.path.dirname(self.csv_path), exist_ok=True)
        series = self._create_series_from_fields(columns_fields)
        self.df = pd.DataFrame(series)
        self._to_csv()

    def _to_csv(self):
        self.df.to_csv(self.csv_path, index=False)

    def _add(self, vehicle_event_entity: VehicleEventEntity) -> None:
        data_dict = asdict(vehicle_event_entity.data)
        flatten_row = asdict(vehicle_event_entity)
        del flatten_row["data"]
        flatten_row.update(data_dict)
        new_row = pd.Series(flatten_row)
        self.df = self.df.append(new_row, ignore_index=True)
        self._writerows(new_row)

    def _writerows(self, row: pd.Series):
        with open(self.csv_path, "a", newline="\n") as file:
            writer = csv.writer(file, delimiter=",")
            writer.writerow(row)

    def add_rows(self, rows: pd.DataFrame):
        self.df = pd.concat([self.df, rows], axis=0)
        self._to_csv()

    def _match_uuid(self, uuid: str) -> Union[VehicleEventEntity, None]:
        _matches = self.df[self.df.uuid == uuid]
        return self._series_to_entity(_matches.iloc[0]) if not _matches.empty else None

    @staticmethod
    def _create_series_from_fields(fields: List[Field]) -> Dict[str, pd.Series]:
        series = {}
        for field in fields:
            dtype = field.type.__name__ if hasattr(field.type, "__name__") else "str"
            if dtype == "DateStr":
                dtype = "datetime64[ns]"
            if dtype == "RawVehicleId":
                dtype = "int"
            series[field.name] = pd.Series([], dtype=dtype)
        return series

    @staticmethod
    def _series_to_entity(series: pd.Series) -> VehicleEventEntity:
        isin_timestamp_uuid_topic = series.index.isin(["timestamp", "uuid", "topic"])
        series_data = series[~isin_timestamp_uuid_topic].to_dict()

        data = VehicleEventData(**series_data)
        entity = VehicleEventEntity(
            data=data,
            timestamp=from_timestamp(series["timestamp"]),
            uuid=series["uuid"],
        )
        return entity

    def resync_current_availability_by_vehicle_raw_id(
        self,
    ) -> AvailabilityKindByVehicleId:
        if self.df.empty:
            return {}
        last_availability = self._get_last_availability_by_id_and_role(self.df)
        return dict(
            zip(last_availability.raw_vehicle_id, last_availability.availability_kind)
        )

    def resync_current_availability_counts_by_role(self) -> CountsByRole:
        if self.df.empty:
            return {role: VehicleAvailability() for role in vehicle_role_options}

        last_availability = self._get_last_availability_by_id_and_role(self.df)
        current_availability_counts_by_role_series = last_availability.groupby(
            ["role", "availability_kind"]
        ).count()
        current_availability_counts_by_role_dict = {}
        for role in current_availability_counts_by_role_series.index.unique(
            level="role"
        ):
            current_availability_counts_by_role_dict[role] = VehicleAvailability(
                **current_availability_counts_by_role_series.loc[role, :].to_dict()[
                    "raw_vehicle_id"
                ]
            )
        return current_availability_counts_by_role_dict

    @staticmethod
    def _get_last_availability_by_id_and_role(df: pd.DataFrame) -> pd.DataFrame:
        """Computes latest status of each vehicles and infer its availability

        Args:
            df (pd.DataFrame): with required columns [raw_vehicle_id: int, role: VehicleRole, status: VehicleStatus]

        Returns:
            pd.DataFrame: with columns [raw_vehicle_id: int, role: VehicleRole, status: VehicleStatus, availability_kind: VehicleAvailabilityKind]

        Examples:

        >> df =
                        raw_vehicle_id                timestamp  home_area  raw_operation_id  ...           role is_available                   topic                                  uuid
                0                   19  2000-01-01 06:00:00.000   264459.0               NaN  ...           pump        False  vehicle_changed_status  e12853cb-0dea-41e1-bd27-6c755dcfa9e4
                534991            2834  2000-01-01 06:00:00.000      617.0               NaN  ...          other         True  vehicle_changed_status  58e13bf1-6fc2-4feb-9fe8-7c40f7090605
                534990            2833  2000-01-01 06:00:00.000      447.0               NaN  ...          other         True  vehicle_changed_status  e0ef2f78-77ef-458a-b357-cca99eb4b683
                534989            2832  2000-01-01 06:00:00.000      448.0               NaN  ...          other         True  vehicle_changed_status  fb73d0a4-addc-4222-a12b-748c6a6c3985
                534988            2831  2000-01-01 06:00:00.000      615.0               NaN  ...          other         True  vehicle_changed_status  ecbfc383-9b4a-4991-9948-72e2eb3a96e8

        >> self._get_last_availability_by_id_and_role(df)
           raw_vehicle_id  role             status availability_kind

            1              other   arrived_at_home         available
            2              other  misc_unavailable       unavailable
            3              other        realocated       recoverable
            4              other   arrived_at_home         available
            5              other       lacks_staff       recoverable
            ...                                ...               ...
            4959           other       lacks_staff       recoverable
            4961           other          selected        in_service
            4962           other       lacks_staff       recoverable
            4963           other          selected        in_service
            4964           other          selected        in_service
        """
        last_availability = (
            df.groupby(["raw_vehicle_id", "role"])
            .apply(lambda s: s.sort_values("timestamp").status.values[-1])
            .to_frame(name="status")
        ).reset_index()
        last_availability["availability_kind"] = last_availability["status"].map(
            vehicle_status_to_availability_kind
        )
        return last_availability
