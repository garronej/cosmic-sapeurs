from dataclasses import asdict

import pandas as pd

from domain.core.domain_event_bus import Event, topic_to_types
from domain.couv_ops.entities.event_entities import EventEntity
from helpers.decorators import timeit


def event_to_series(event: Event) -> pd.Series:
    event_dict = asdict(event.data)
    event_dict["uuid"] = event.uuid
    event_dict["timestamp"] = event.timestamp
    event_dict["topic"] = event.topic
    return pd.Series(event_dict)


def event_entity_to_series(event_entity: EventEntity) -> pd.Series:
    event_entity_dict = asdict(event_entity.data)
    event_entity_dict["uuid"] = event_entity.uuid
    event_entity_dict["timestamp"] = event_entity.timestamp
    return pd.Series(event_entity_dict)


def series_to_event(series: pd.Series) -> Event:
    data_dict = dict(series[~series.index.isin(["timestamp", "uuid", "topic"])])
    topic = series.topic
    if topic not in topic_to_types:
        raise ValueError(f"Unknown topic {topic}")
    EventDataType = topic_to_types.get(topic)["data"]
    EventType = topic_to_types.get(topic)["event"]
    data = EventDataType(**data_dict)
    return EventType(
        timestamp=series.timestamp, topic=series.topic, uuid=series.uuid, data=data
    )
