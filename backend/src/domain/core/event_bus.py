import abc
import asyncio
from collections import defaultdict
from dataclasses import asdict, dataclass
from typing import Any, Callable, Coroutine, Dict, Generic, List, TypeVar

from helpers.date import DateStr

EventCallback = Callable[[Any], Coroutine[Any, Any, Any]]
GenericTopic = TypeVar("GenericTopic")


@dataclass
class Event(Generic[GenericTopic]):
    timestamp: DateStr
    uuid: str
    topic: GenericTopic
    data: Any = None

    def asdict(self):
        return {
            "timestamp": str(self.timestamp),
            "topic": self.topic,
            "uuid": self.uuid,
            "data": asdict(self.data),
        }


class AbstractEventBus(abc.ABC, Generic[GenericTopic]):
    @abc.abstractclassmethod
    def subscribe(self, topic: GenericTopic, callback: EventCallback) -> None:
        raise NotImplementedError()


class InMemoryEventBus(AbstractEventBus, Generic[GenericTopic]):
    def __init__(self) -> None:
        super().__init__()
        Subscriptions = Dict[GenericTopic, List[EventCallback]]
        self._subscriptions: Subscriptions = defaultdict(lambda: [])

    def subscribe(self, topic: GenericTopic, callback: EventCallback) -> None:
        self._subscriptions[topic].append(callback)

    async def publish(self, event: Event[GenericTopic]) -> None:
        if self._subscriptions[event.topic]:
            await asyncio.wait(
                [callback(event) for callback in self._subscriptions[event.topic]]
            )
