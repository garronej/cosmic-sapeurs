from dataclasses import dataclass
from typing import Any, Callable, Coroutine, Dict, Literal, TypedDict, Union

from pandas import Timestamp

from domain.core.event_bus import Event, InMemoryEventBus
from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    CouvOpsUpdatedEventData,
)
from domain.couv_ops.data_transfert_objects.vehicle_event_data import VehicleEventData


class InvalidTopic(Exception):
    pass


TopicVehicleChangedStatus = Literal["vehicle_changed_status"]
TopicCouvOpsUpdated = Literal["vehicleAvailabilityChanged"]
TopicReplayedEventBusBecameReady = Literal["replayedEventsBusBecameReady"]
DomainTopic = Union[
    TopicVehicleChangedStatus, TopicCouvOpsUpdated, TopicReplayedEventBusBecameReady
]
EventData = Union[VehicleEventData, CouvOpsUpdatedEventData]
EventCallback = Callable[[Any], Coroutine[Any, Any, Any]]


class DictWithKeysDataEvent(TypedDict):
    data: Any  #  Should be Data
    event: Any  # Should be Event


@dataclass
class VehicleEvent(Event):
    topic: TopicVehicleChangedStatus
    data: VehicleEventData


DomainEventBus = InMemoryEventBus[DomainTopic]

topic_to_types: Dict[DomainTopic, DictWithKeysDataEvent] = {
    "vehicle_changed_status": {
        "data": VehicleEventData,
        "event": VehicleEvent,
    }
}
