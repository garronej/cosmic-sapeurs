import abc
from itertools import groupby
from typing import Dict, List, Union

from tqdm import tqdm

from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    VehicleAvailability,
)
from domain.couv_ops.data_transfert_objects.vehicle_event_data import (
    RawVehicleId,
    VehicleAvailabilityKind,
    VehicleRole,
    vehicle_availability_kind_options,
    vehicle_role_options,
)
from domain.couv_ops.entities.event_entities import VehicleEventEntity
from domain.couv_ops.helpers.event_entity_converters import event_to_entity

VehicleEventsList = List[VehicleEventEntity]


class AlreadyExistingVehicleEventUuid(Exception):
    pass


CountsByRole = Dict[VehicleRole, VehicleAvailability]

AvailabilityKindByVehicleId = Dict[RawVehicleId, VehicleAvailabilityKind]


def default_count_by_role_dict() -> CountsByRole:
    count_by_role: CountsByRole = {}
    for vehicle_role in vehicle_role_options:
        count_by_role[vehicle_role] = VehicleAvailability()
    return count_by_role


class AbstractVehicleEventsRepository(abc.ABC):
    def add(self, vehicle_event_entity: VehicleEventEntity) -> None:
        if self._match_uuid(vehicle_event_entity.uuid):
            raise AlreadyExistingVehicleEventUuid()
        self._add(vehicle_event_entity)

    @abc.abstractclassmethod
    def resync_current_availability_by_vehicle_raw_id(
        self,
    ) -> AvailabilityKindByVehicleId:
        raise NotImplementedError

    @abc.abstractclassmethod
    def resync_current_availability_counts_by_role(self) -> CountsByRole:
        raise NotImplementedError

    # @abc.abstractclassmethod
    def save_events_batch(self, events_batch: List[VehicleEventEntity]):
        for event in tqdm(events_batch):
            self.add(event_to_entity(event))

    @abc.abstractclassmethod
    def _add(self, vehicle_event_entity: VehicleEventEntity) -> None:
        raise NotImplementedError

    @abc.abstractclassmethod
    def _match_uuid(self, uuid: str) -> Union[VehicleEventEntity, None]:
        raise NotImplementedError


class InMemoryVehicleEventRepository(AbstractVehicleEventsRepository):
    def __init__(self) -> None:
        self._vehicle_events: VehicleEventsList = []
        super().__init__()

    def get_all(self) -> VehicleEventsList:
        return self._vehicle_events

    def resync_current_availability_by_vehicle_raw_id(
        self,
    ) -> AvailabilityKindByVehicleId:
        current_availability: AvailabilityKindByVehicleId = {}
        # Group by vehicle id and order by timestamp
        for raw_vehicle_id, group in groupby(
            sorted(
                self._vehicle_events,
                key=lambda event: event.data.raw_vehicle_id,
            ),
            lambda event: event.data.raw_vehicle_id,
        ):
            group_sorted_by_timestamp = list(
                sorted(
                    group,
                    key=lambda event: event.timestamp,
                ),
            )
            latest_event_data = group_sorted_by_timestamp[-1].data
            current_availability[raw_vehicle_id] = latest_event_data.availability_kind
        return current_availability

    def resync_current_availability_counts_by_role(self) -> CountsByRole:

        counts_dict_by_role = {
            role: {
                availability_kind: 0
                for availability_kind in vehicle_availability_kind_options
            }
            for role in vehicle_role_options
        }

        # Group by vehicle id and order by timestamp
        for _, group in groupby(
            sorted(
                self._vehicle_events,
                key=lambda event: event.data.raw_vehicle_id,
            ),
            lambda event: event.data.raw_vehicle_id,
        ):
            group_sorted_by_timestamp = list(
                sorted(
                    group,
                    key=lambda event: event.timestamp,
                ),
            )
            latest_event_data = group_sorted_by_timestamp[-1].data

            counts_dict_by_role[latest_event_data.role][
                latest_event_data.availability_kind
            ] += 1

        counts_by_role = {}
        for role in vehicle_role_options:
            counts_by_role[role] = VehicleAvailability(**counts_dict_by_role[role])

        return counts_by_role

    def _match_uuid(self, uuid: str) -> Union[VehicleEventEntity, None]:
        _matches = [event for event in self._vehicle_events if event.uuid == uuid]
        return _matches[0] if _matches else None

    def _add(self, vehicle_event_entity: VehicleEventEntity):
        self._vehicle_events.append(vehicle_event_entity)

    # next methods are only for test purposes
    @property
    def vehicle_events(self) -> VehicleEventsList:
        return self._vehicle_events

    @vehicle_events.setter
    def vehicle_events(self, vehicle_events: List[VehicleEventEntity]):
        self._vehicle_events = vehicle_events
