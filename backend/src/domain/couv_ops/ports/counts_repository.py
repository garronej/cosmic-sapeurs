import abc
from abc import abstractproperty
from typing import List, Union

from domain.couv_ops.entities.event_entities import CouvOpsUpdatedEventEntity
from helpers.decorators import timeit

CountList = List[CouvOpsUpdatedEventEntity]


class AbstractCountsRepository(abc.ABC):
    def add(self, count_entity: CouvOpsUpdatedEventEntity):
        self._add(count_entity)

    @abc.abstractclassmethod
    def _add(self, count: CouvOpsUpdatedEventEntity) -> None:
        raise NotImplementedError

    @abstractproperty
    def last_count(self):
        raise NotImplementedError


class InMemoryCountsRepository(AbstractCountsRepository):
    def __init__(self) -> None:
        self._counts: CountList = []

    def _add(self, count_entity: CouvOpsUpdatedEventEntity):
        self._counts.append(count_entity)

    @property
    def counts(self) -> CountList:
        return self._counts

    @property
    def last_count(self) -> Union[CouvOpsUpdatedEventEntity, None]:
        return self._counts[-1] if self._counts else None
