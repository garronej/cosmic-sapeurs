from dataclasses import dataclass

from domain.core.domain_event_bus import TopicCouvOpsUpdated, TopicVehicleChangedStatus
from domain.core.event_bus import Event
from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    CouvOpsUpdatedEventData,
)
from domain.couv_ops.data_transfert_objects.vehicle_event_data import VehicleEventData


@dataclass
class VehicleEvent(Event[TopicVehicleChangedStatus]):
    data: VehicleEventData
    topic: TopicVehicleChangedStatus = "vehicle_changed_status"


@dataclass
class CouvOpsUpdatedEvent(Event[TopicCouvOpsUpdated]):
    data: CouvOpsUpdatedEventData
    topic: TopicCouvOpsUpdated = "vehicleAvailabilityChanged"
