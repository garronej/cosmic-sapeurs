from dataclasses import dataclass

from dataclasses_jsonschema import JsonSchemaMixin

from domain.couv_ops.data_transfert_objects.vehicle_event_data import VehicleRole


@dataclass
class VehicleAvailability(JsonSchemaMixin, allow_additional_props=False):
    available: int = 0
    unavailable: int = 0
    in_service: int = 0
    recoverable: int = 0


@dataclass
class CouvOpsUpdatedEventData(JsonSchemaMixin, allow_additional_props=False):
    counts: VehicleAvailability
    role: VehicleRole
