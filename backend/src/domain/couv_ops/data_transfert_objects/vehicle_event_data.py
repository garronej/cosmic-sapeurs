from dataclasses import dataclass
from typing import Dict, List, Literal, NewType

from dataclasses_jsonschema import JsonSchemaMixin

VehicleAvailabilityKind = Literal[
    "available", "unavailable", "recoverable", "in_service"
]
vehicle_availability_kind_options: List[VehicleAvailabilityKind] = [
    "available",
    "unavailable",
    "recoverable",
    "in_service",
]

VehicleStatus = Literal[
    "departed_to_intervention",
    "arrived_on_intervention",
    "transport_to_hospital",
    "arrived_at_hospital",
    "left_hospital",
    "recoverable_within_15_minutes",  # sport
    "selected",  # présentation spontannée, sélection / instance de départ
    "waiting",
    "broken",
    "lacks_staff",  # manque personnel, omnibus
    "cancelled",
    "set_aside",  # indispo délestage, indispo 1er départ
    "realocated",  # monté en garde
    "undefined",  # or unused
    "misc_unavailable",
    "misc_available",
    "arrived_at_home",
]
vehicle_status_options: List[VehicleStatus] = [
    "departed_to_intervention",
    "arrived_on_intervention",
    "transport_to_hospital",
    "arrived_at_hospital",
    "left_hospital",
    "recoverable_within_15_minutes",
    "selected",
    "waiting",
    "broken",
    "lacks_staff",
    "cancelled",
    "set_aside",
    "realocated",
    "undefined",
    "misc_unavailable",
    "misc_available",
    "arrived_at_home",
]

vehicle_status_to_availability_kind: Dict[VehicleStatus, VehicleAvailabilityKind] = {
    "departed_to_intervention": "in_service",
    "arrived_on_intervention": "in_service",
    "transport_to_hospital": "in_service",
    "arrived_at_hospital": "in_service",
    "left_hospital": "in_service",
    "recoverable_within_15_minutes": "recoverable",
    "selected": "in_service",  # présentation spontannée, sélection / instance de départ
    "waiting": "in_service",
    "broken": "unavailable",
    "lacks_staff": "recoverable",  # manque personnel, omnibus
    "cancelled": "available",
    "set_aside": "recoverable",  # indispo délestage, indispo 1er départ
    "realocated": "recoverable",  # monté en garde
    "undefined": "unavailable",  # or unused
    "misc_unavailable": "unavailable",
    "misc_available": "available",
    "arrived_at_home": "available",
}

VehicleRole = Literal["victim_rescue", "pump", "other"]
vehicle_role_options: List[VehicleRole] = ["victim_rescue", "pump", "other"]

RawVehicleId = NewType("RawVehicleId", int)


def vehicle_is_available(status: VehicleStatus) -> bool:
    return vehicle_status_to_availability_kind.get(status) == "available"


@dataclass
class VehicleEventData(
    JsonSchemaMixin, allow_additional_props=False, serialise_properties=True
):
    raw_vehicle_id: RawVehicleId  # 'Operation-Software'  vehicle ID
    raw_operation_id: int  # inter = op ?  affaire ? # 'Operation-Software'  intervention ID
    status: VehicleStatus
    # is_available: bool
    # availability_kind: VehicleAvailabilityKind
    raw_status: str  # Status label from 'Operation-Software'
    home_area: str  # Reference on a partitioned map of where the vehicle belongs
    role: VehicleRole

    @property
    def is_available(self) -> bool:
        return vehicle_is_available(self.status)

    @property
    def availability_kind(self) -> VehicleAvailabilityKind:
        return vehicle_status_to_availability_kind[self.status]
