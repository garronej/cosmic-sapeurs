from dataclasses import dataclass

from dataclasses_jsonschema import JsonSchemaMixin

from domain.couv_ops.data_transfert_objects.vehicle_event_data import (
    VehicleAvailabilityKind,
    VehicleRole,
    VehicleStatus,
)


@dataclass
class Litterals(JsonSchemaMixin, allow_additional_props=False):
    vehicleRole: VehicleRole
    vehicleStatus: VehicleStatus
    vehicleAvailabilityKind: VehicleAvailabilityKind
