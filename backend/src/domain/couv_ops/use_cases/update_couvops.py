from typing import Union

from pandas._libs.tslibs import Timestamp

from domain.core.domain_event_bus import DomainEventBus
from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    CouvOpsUpdatedEventData,
    VehicleAvailability,
)
from domain.couv_ops.data_transfert_objects.vehicle_event_data import (
    VehicleAvailabilityKind,
    VehicleRole,
)
from domain.couv_ops.entities.event_entities import (
    CouvOpsUpdatedEventEntity,
    VehicleEventEntity,
)
from domain.couv_ops.events.events import CouvOpsUpdatedEvent, VehicleEvent
from domain.couv_ops.ports.counts_repository import AbstractCountsRepository
from domain.couv_ops.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
)
from helpers.decorators import async_timeit, timeit
from helpers.uuid import AbstractUuid


class UnknownRoleError(Exception):
    pass


class UpdateCouvops:
    def __init__(
        self,
        vehicle_event_repo: AbstractVehicleEventsRepository,
        counts_repo: AbstractCountsRepository,
        domain_event_bus: DomainEventBus,
        uuid: AbstractUuid,
    ) -> None:
        self.domain_event_bus = domain_event_bus
        self.vehicle_repo = vehicle_event_repo
        self.counts_repo = counts_repo
        self.uuid = uuid
        self.resync_from_vehicle_repo()

    def resync_from_vehicle_repo(self):
        self.current_counts_by_role = (
            self.vehicle_repo.resync_current_availability_counts_by_role()
        )
        self.current_availability_by_vehicle = (
            self.vehicle_repo.resync_current_availability_by_vehicle_raw_id()
        )

    async def execute(self, event: VehicleEvent) -> None:
        """Store event in repository

        Args:
            event (VehicleEvent): event published on vehicle change
        """

        vehicle_role = event.data.role

        if vehicle_role not in self.current_counts_by_role:
            raise UnknownRoleError(
                f"Received vehicle event with unknown role : {vehicle_role}"
            )

        current_availability_kind = event.data.availability_kind

        previous_available_kind = self.current_availability_by_vehicle.get(
            event.data.raw_vehicle_id
        )
        # status is set to "unavailable" when vehicle is new

        if previous_available_kind == current_availability_kind:
            return

        new_counts_for_role = self._get_available_count_for_role(
            current_role=vehicle_role,
            previous_available_kind=previous_available_kind,
            current_availability_kind=current_availability_kind,
        )

        count_entity = CouvOpsUpdatedEventEntity(
            timestamp=event.timestamp,
            uuid=self.uuid.make(),
            data=CouvOpsUpdatedEventData(
                role=vehicle_role,
                counts=new_counts_for_role,
            ),
        )
        self.counts_repo.add(count_entity)

        self.current_counts_by_role[vehicle_role] = new_counts_for_role

        self.current_availability_by_vehicle[
            event.data.raw_vehicle_id
        ] = event.data.availability_kind

        couv_ops_updated_event = CouvOpsUpdatedEvent(
            data=count_entity.data,
            timestamp=count_entity.timestamp,
            uuid=self.uuid.make(),
        )

        await self.domain_event_bus.publish(couv_ops_updated_event)

    def _get_available_count_for_role(
        self,
        current_role: VehicleRole,
        previous_available_kind: Union[VehicleAvailabilityKind, None],
        current_availability_kind: VehicleAvailabilityKind,
    ) -> VehicleAvailability:
        current_count_for_role = self.current_counts_by_role[current_role]
        count_delta_for_role = {
            previous_available_kind: -1 if previous_available_kind else 0,
            current_availability_kind: +1,
        }
        return VehicleAvailability(
            available=current_count_for_role.available
            + count_delta_for_role.get("available", 0),
            unavailable=current_count_for_role.unavailable
            + count_delta_for_role.get("unavailable", 0),
            recoverable=current_count_for_role.recoverable
            + count_delta_for_role.get("recoverable", 0),
            in_service=current_count_for_role.in_service
            + count_delta_for_role.get("in_service", 0),
        )
