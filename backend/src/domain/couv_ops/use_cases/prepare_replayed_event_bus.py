import time

from adapters.pandas.pandas_vehicle_events_repository import (
    PandasVehicleEventsRepository,
)
from adapters.scheduled.scheduled_event_bus import ReplayedEventBus
from domain.core.domain_event_bus import DomainEventBus
from domain.core.event_bus import Event
from helpers.clock import AbstractClock
from helpers.date import DateStr, from_datetime, to_timestamp
from helpers.decorators import timeit
from helpers.uuid import uuid4


class PrepareReplayedEventBus:
    def __init__(
        self,
        event_bus: ReplayedEventBus,
        vehicle_events_repo: PandasVehicleEventsRepository,
        clock: AbstractClock,
        domain_event_bus: DomainEventBus,
    ) -> None:
        self.event_bus = event_bus
        self.vehicle_events_repo = vehicle_events_repo
        self.clock = clock
        self.domain_event_bus = domain_event_bus

    @timeit(func_name="PrepareReplayedEventBus.execute")
    async def execute(self, date: DateStr):
        rows_to_tranfert = self.event_bus.get_events_between(
            start_date=None, end_date=date
        )
        print(f"Found {len(rows_to_tranfert)} events to transfert to repo")
        self.vehicle_events_repo.add_rows(rows_to_tranfert)
        self.event_bus.set_last_published_timestamp(date)
        bus_became_ready_event = Event(
            topic="replayedEventsBusBecameReady",
            # todo: get rid of timestamp (?)
            timestamp=self.clock.get_now(),
            uuid=uuid4(),
        )
        await self.domain_event_bus.publish(bus_became_ready_event)
