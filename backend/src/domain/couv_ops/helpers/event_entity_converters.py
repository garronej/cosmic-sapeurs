from domain.core.event_bus import Event
from domain.couv_ops.entities.event_entities import EventEntity


def event_to_entity(event: Event) -> EventEntity:
    return EventEntity(timestamp=event.timestamp, uuid=event.uuid, data=event.data)
