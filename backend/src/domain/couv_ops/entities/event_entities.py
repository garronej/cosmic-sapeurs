import abc
from dataclasses import dataclass

from domain.core.domain_event_bus import EventData
from domain.couv_ops.data_transfert_objects.couv_ops_event_data import (
    CouvOpsUpdatedEventData,
)
from domain.couv_ops.data_transfert_objects.vehicle_event_data import VehicleEventData
from helpers.date import DateStr


@dataclass
class EventEntity(abc.ABC):
    timestamp: DateStr
    data: EventData
    uuid: str


class VehicleEventEntity(EventEntity):
    data: VehicleEventData


@dataclass
class CouvOpsUpdatedEventEntity(EventEntity):
    data: CouvOpsUpdatedEventData
