import os
from typing import Callable, Dict

from dotenv import load_dotenv

from adapters.pandas.pandas_vehicle_events_repository import (
    PandasVehicleEventsRepository,
)
from adapters.scheduled.scheduled_event_bus import (
    RandomScheduledEventBus,
    ReplayedEventBus,
    ScheduledEventBus,
)
from domain.core.domain_event_bus import DomainEventBus
from domain.couv_ops.ports.counts_repository import (
    AbstractCountsRepository,
    InMemoryCountsRepository,
)
from domain.couv_ops.ports.vehicle_events_repository import (
    AbstractVehicleEventsRepository,
    InMemoryVehicleEventRepository,
)
from domain.couv_ops.use_cases.prepare_replayed_event_bus import PrepareReplayedEventBus
from domain.couv_ops.use_cases.update_couvops import UpdateCouvops
from helpers.clock import RealClock
from helpers.uuid import AbstractUuid, RealUuid

load_dotenv()

vehicle_event_repo = InMemoryVehicleEventRepository()
counts_repo = InMemoryCountsRepository()

clock = RealClock()

env_var_to_event_bus: Dict[str, Callable[[], ScheduledEventBus]] = dict(
    RANDOM=lambda: RandomScheduledEventBus(
        clock=clock, min_time_step=0.1, max_time_step=2
    ),
    PANDAS=lambda: ReplayedEventBus(
        clock=clock,
        csv_path="./data/vehicule_events.csv",
        time_step=3.0,
        resync=False,
        speed=100,
    ),
)


class Config:
    scheduled_event_bus: ScheduledEventBus
    vehicle_event_repo: AbstractVehicleEventsRepository
    counts_repo: AbstractCountsRepository
    domain_event_bus: DomainEventBus
    uuid: AbstractUuid

    def __init__(self) -> None:
        # repo_infra = os.environ.get("REPOSITORIES")
        event_bus_infra = os.environ.get("SCHEDULED_EVENT_BUS", "RANDOM")
        self.clock = clock
        self.domain_event_bus = DomainEventBus()
        self.scheduled_event_bus = env_var_to_event_bus[event_bus_infra]()
        csv_path = "data/replay_sept20.csv"
        self.vehicle_event_repo = PandasVehicleEventsRepository(csv_path)
        self.vehicle_event_repo.init_empty_df()
        # InMemoryVehicleEventRepository()
        self.counts_repo = InMemoryCountsRepository()
        self.uuid = RealUuid()

    def use_cases(self):
        return UpdateCouvops(
            vehicle_event_repo=self.vehicle_event_repo,
            counts_repo=self.counts_repo,
            domain_event_bus=self.domain_event_bus,
            uuid=self.uuid,
        ), PrepareReplayedEventBus(
            clock=self.clock,
            domain_event_bus=self.domain_event_bus,
            event_bus=self.scheduled_event_bus,
            vehicle_events_repo=self.vehicle_event_repo,
        )
