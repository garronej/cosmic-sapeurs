import json
from dataclasses import asdict, is_dataclass
from typing import Callable

import aiohttp
from aiohttp import web

from domain.couv_ops.events.events import CouvOpsUpdatedEvent
from entrypoints.commands import Command


class SapeurWebSocketResponse(web.WebSocketResponse):
    def __init__(self, command_handlers: Callable[[Command], None]) -> None:
        super().__init__()
        self.command_handlers = command_handlers

    async def send_to_front(self, event: CouvOpsUpdatedEvent):
        # todo : check if data is serializable, and eventually convert it
        await self.send_json(
            {
                "timestamp": str(event.timestamp),
                "topic": event.topic,
                "payload": asdict(event.data)
                if is_dataclass(event.data)
                else event.data,
            }
        )

    async def run(self):
        async for msg in self:
            print("msg is : ", msg)
            if msg.type == aiohttp.WSMsgType.TEXT:
                if msg.data == "close":
                    await self.close()
                else:
                    self.command_handlers(json.loads(msg.data))
            elif msg.type == aiohttp.WSMsgType.ERROR:
                print("ws connection closed with exception %s" % self.exception())
        print("websocket connection closed")
