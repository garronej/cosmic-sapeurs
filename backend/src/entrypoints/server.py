import asyncio

import aiohttp_cors
from aiohttp import web

from domain.core.domain_event_bus import VehicleEvent
from entrypoints.commands import Command
from entrypoints.config import Config
from entrypoints.socket_handlers import SapeurWebSocketResponse
from helpers.date import DateStr

routes = web.RouteTableDef()

config = Config()


@routes.get("/hello")
async def hello(request):
    return web.json_response({"message": "Hello, world"})


update_couvops, prepare_replayed_event_bus = config.use_cases()

scheduled_event_bus = config.scheduled_event_bus


async def publish_in_domain(e: VehicleEvent):
    await config.domain_event_bus.publish(e)


async def resync_update_couvops_from_vehicle_repo(e):
    update_couvops.resync_from_vehicle_repo()


config.scheduled_event_bus.subscribe("vehicle_changed_status", publish_in_domain)

config.domain_event_bus.subscribe("vehicle_changed_status", update_couvops.execute)

config.domain_event_bus.subscribe(
    "replayedEventsBusBecameReady", resync_update_couvops_from_vehicle_repo
)


@routes.get("/ws")
async def websocket_handler(request):
    # commands from frontend
    def command_handlers(command: Command):
        print("\n \n command: ", command)
        if command["topic"] == "startReplayedEvents":
            scheduled_event_bus.start()
            loop.create_task(scheduled_event_bus.async_loop())
        elif command["topic"] == "setReplayedFromDate":
            loop.create_task(
                prepare_replayed_event_bus.execute(date=DateStr(command["payload"]))
            )
        else:
            print("-->>>>  message received data, command not handled : ", command)

    ws = SapeurWebSocketResponse(command_handlers)

    # events to send to frontend
    config.domain_event_bus.subscribe("vehicleAvailabilityChanged", ws.send_to_front)
    config.domain_event_bus.subscribe("replayedEventsBusBecameReady", ws.send_to_front)

    await ws.prepare(request)
    await ws.run()

    return ws


app = web.Application()
app.add_routes(routes)

cors = aiohttp_cors.setup(
    app,
    defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    },
)

for route in list(app.router.routes()):
    cors.add(route)

loop = asyncio.get_event_loop()

runner = web.AppRunner(app)
loop.run_until_complete(runner.setup())
site = web.TCPSite(runner)
loop.run_until_complete(site.start())

loop.run_forever()
