from datetime import datetime
from typing import Literal, TypedDict, Union

CommandTopic = Literal["startScheduledEvents", "setReplayedFromDate"]


class StartReplayedEventsCommand(TypedDict):
    topic: CommandTopic


class SetReplayedFromDateCommand(TypedDict):
    topic: CommandTopic
    payload: datetime


Command = Union[StartReplayedEventsCommand, SetReplayedFromDateCommand]
