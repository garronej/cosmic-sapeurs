import uuid as uuid_lib
from abc import ABC, abstractclassmethod


def uuid4() -> str:
    return str(uuid_lib.uuid4())


class AbstractUuid(ABC):
    @abstractclassmethod
    def make() -> str:
        raise NotImplementedError


class CustomUuid(AbstractUuid):
    def __init__(self, uuid: str = None) -> None:
        self._next_uuid = uuid or uuid4()

    def make(self) -> str:
        return self._next_uuid

    def set_next_uuid(self, uuid: str):
        self._next_uuid = uuid


class RealUuid(AbstractUuid):
    def make(self) -> str:
        return uuid4()
