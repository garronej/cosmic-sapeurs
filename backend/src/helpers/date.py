from datetime import datetime
from typing import NewType

from pandas._libs.tslibs import Timestamp

DateStr = NewType("DateStr", str)

date_format = "%Y-%m-%dT%H:%M:%S"


def to_timestamp(date: DateStr) -> Timestamp:
    return Timestamp(date).tz_localize(None)


def from_timestamp(timestamp: Timestamp) -> DateStr:
    return DateStr(str(timestamp))


def from_datetime(date: datetime) -> DateStr:
    return DateStr(date.strftime(date_format))


def to_datetime(date: DateStr) -> datetime:
    return datetime.strptime(date, date_format)
