import random
from dataclasses import asdict
from typing import Dict, List, Union

from faker import Faker

from domain.couv_ops.data_transfert_objects.vehicle_event_data import (
    RawVehicleId,
    VehicleEventData,
    VehicleRole,
    VehicleStatus,
    vehicle_is_available,
    vehicle_role_options,
    vehicle_status_options,
)
from domain.couv_ops.entities.event_entities import VehicleEventEntity
from domain.couv_ops.events.events import VehicleEvent
from helpers.date import DateStr
from helpers.uuid import uuid4

fake = Faker()

default_timestamp = DateStr("2020-01-01T12")
# default_uuid = uuid4()

vehicle_ids_by_role: Dict[VehicleRole, List[RawVehicleId]] = {}
for k, vehicle_role in enumerate(vehicle_role_options):
    vehicle_ids_by_role[vehicle_role] = [
        RawVehicleId(vehicle_id) for vehicle_id in range(k * 20, (k + 1) * 20)
    ]


def random_id_by_role(role: VehicleRole) -> RawVehicleId:
    ids = vehicle_ids_by_role[role]
    return random.choice(ids)


def make_vehicle_event_data(
    raw_vehicle_id: Union[RawVehicleId, int] = None,
    raw_operation_id: int = None,
    status: VehicleStatus = None,
    raw_status: str = None,
    home_area: str = None,
    role: VehicleRole = None,
) -> VehicleEventData:
    if status and status not in vehicle_status_options:
        raise ValueError(f"Provided status not in {vehicle_status_options}")
    raw_operation_id = raw_operation_id or fake.pyint()
    status = status or (random.choice(vehicle_status_options))
    raw_status = raw_status or "some raw status"
    home_area = (
        home_area or "some home area"
    )  # warnings: this should be an id linked to the map
    role = role or random.choice(vehicle_role_options)
    new_raw_vehicle_id: RawVehicleId = (
        RawVehicleId(raw_vehicle_id) if raw_vehicle_id else random_id_by_role(role)
    )

    vehicle_event_data = VehicleEventData(
        raw_vehicle_id=new_raw_vehicle_id,
        raw_operation_id=raw_operation_id,
        status=status,
        raw_status=raw_status,
        home_area=home_area,
        role=role,
    )
    return vehicle_event_data


def make_vehicle_event(
    timestamp: DateStr = None,
    uuid: str = None,
    # Following args are for make_vehicle_event_data
    raw_vehicle_id: Union[RawVehicleId, int] = None,
    raw_operation_id: int = None,
    status: VehicleStatus = None,
    raw_status: str = None,
    home_area: str = None,
    role: VehicleRole = None,
) -> VehicleEvent:
    vehicle_event_data = make_vehicle_event_data(
        raw_vehicle_id=raw_vehicle_id,
        raw_operation_id=raw_operation_id,
        status=status,
        raw_status=raw_status,
        home_area=home_area,
        role=role,
    )
    timestamp = timestamp or default_timestamp
    uuid = uuid or uuid4()
    return VehicleEvent(
        timestamp=timestamp,
        uuid=uuid,
        data=vehicle_event_data,
    )


def make_vehicle_event_entity(
    # TODO : replace data with actual make_vehicle_event_data params
    timestamp: DateStr = None,
    uuid: str = None,
    # data: VehicleEventData = None,
    # Following args are for make_vehicle_event_data
    raw_vehicle_id: Union[RawVehicleId, int] = None,
    raw_operation_id: int = None,
    status: VehicleStatus = None,
    raw_status: str = None,
    home_area: str = None,
    role: VehicleRole = None,
) -> VehicleEventEntity:
    # vehicle_event_data = data or make_vehicle_event_data()
    vehicle_event_data = make_vehicle_event_data(
        raw_vehicle_id=raw_vehicle_id,
        raw_operation_id=raw_operation_id,
        status=status,
        raw_status=raw_status,
        home_area=home_area,
        role=role,
    )
    timestamp = timestamp or default_timestamp
    uuid = uuid or uuid4()
    return VehicleEventEntity(timestamp=timestamp, uuid=uuid, data=vehicle_event_data)
