import asyncio
from functools import wraps
from time import time
from typing import Callable


def timeit(func_name: str = "function"):
    def _timeit_decorator(func: Callable):
        @wraps(func)
        def _timeit_wrapper(*args, **kwargs):
            start = time()
            try:
                return func(*args, **kwargs)
            finally:
                print(f"[{func_name}] Execution took {(time() - start) * 1000} ms")

        return _timeit_wrapper

    return _timeit_decorator


def async_timeit(func_name: str = "function"):
    """ See : https://gist.github.com/Integralist/77d73b2380e4645b564c28c53fae71fb """

    def _timeit_decorator(func):
        async def process(func, *args, **params):
            if asyncio.iscoroutinefunction(func):
                return await func(*args, **params)
            else:
                return func(*args, **params)

        async def helper(*args, **params):
            start = time()
            result = await process(func, *args, **params)

            # Test normal function route...
            # result = await process(lambda *a, **p: print(*a, **p), *args, **params)

            print(f"[{func_name}] Async execution took {(time() - start) * 1000} ms")
            return result

        return helper

    return _timeit_decorator
