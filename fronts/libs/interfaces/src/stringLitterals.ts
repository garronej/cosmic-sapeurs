// autre fichier :

import { Litterals } from "./generated/Litterals";

export type VehicleStatus = Litterals["vehicleStatus"];
export type VehicleRole = Litterals["vehicleRole"];
export type VehicleAvailablilityKind = Litterals["vehicleAvailabilityKind"];
