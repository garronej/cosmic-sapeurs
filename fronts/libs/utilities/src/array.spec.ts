import { replaceAt, replaceLast } from "./array";

describe("ReplaceLast utils", () => {
  it("Replaces last value when array is of length 1", () => {
    expect(replaceLast(["last"], "newLast")).toEqual(["newLast"]);
  });
  it("Replaces last value when array is of length 3", () => {
    expect(replaceLast(["first", "second", "last"], "newLast")).toEqual([
      "first",
      "second",
      "newLast",
    ]);
  });
});

describe("ReplaceAt utils", () => {
  it("Replaces first value with array of length 1", () => {
    expect(replaceAt(["last"], 0, "newLast")).toEqual(["newLast"]);
  });
  it("Replaces third value when array is of length 4", () => {
    expect(
      replaceAt(["first", "second", "third", "forth"], 2, "newThird"),
    ).toEqual(["first", "second", "newThird", "forth"]);
  });
  it("?? if index out of range", () => {
    expect(
      replaceAt(["first", "second", "third", "forth"], 8, "outOfRange"),
    ).toEqual(["first", "second", "third", "forth"]);
  });
});
