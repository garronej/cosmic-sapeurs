export const replaceAt = <T>(array: T[], index: number, newValue: T) => {
  if (index > array.length) {
    console.warn("Index greater than array length. ");
    return array;
  }
  return [...array.slice(0, index), newValue, ...array.slice(index + 1)];
};

export const replaceLast = <T>(array: T[], newValue: T) => {
  if (array.length == 1) return [newValue];
  return [...array.slice(0, -1), newValue];
};
