import { initiateScheduledEventsThunk } from "../useCases/couvOps/initiateScheduledEvents.thunk";
import { setReplayedFromDateThunk } from "../useCases/couvOps/setReplayedFromDate.thunk";
import { helloSapeurThunk } from "../useCases/helloSapeur/helloSapeur.thunk";
import { subscribeToVehicleAvailableCountThunk } from "../useCases/couvOps/subscribeToVehicleAvailableCount.thunk";

export const actions = {
  helloSapeurThunk,
  subscribeToVehicleAvailableCountThunk,
  initiateScheduledEventsThunk,
  setReplayedFromDateThunk,
};
