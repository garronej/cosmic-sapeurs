import { ActualHttpClient } from "../secondaryAdapters/ActualHttpClient";
import { InMemoryHttpClient } from "../secondaryAdapters/InMemoryHttpClient";
import { InMemoryPubSubClient } from "../secondaryAdapters/InMemoryPubSubClient";
import { WebSocketPubSubClient } from "../secondaryAdapters/WebSocketPubSubClient";
import { configureReduxStore } from "./store.config";

type GetStoreProps = {
  httpClientKind: "IN_MEMORY" | "HTTP";
  pubSubClientKind: "IN_MEMORY" | "WEB_SOCKET";
  backendUrl: string;
  socketUrl: string;
};

export const getStore = ({
  httpClientKind,
  pubSubClientKind,
  backendUrl,
  socketUrl,
}: GetStoreProps) => {
  const httpClient =
    httpClientKind === "IN_MEMORY"
      ? new InMemoryHttpClient(500)
      : new ActualHttpClient(backendUrl);

  const pubSubClient =
    pubSubClientKind === "IN_MEMORY"
      ? new InMemoryPubSubClient()
      : new WebSocketPubSubClient(socketUrl);

  return configureReduxStore({ httpClient, pubSubClient });
};
