import { combineReducers } from "@reduxjs/toolkit";
import { couvOpsReducer } from "../useCases/couvOps/couvOps.slice";
import { helloSapeurReducer } from "../useCases/helloSapeur/helloSapeur.slice";

export const rootReducer = combineReducers({
  helloSapeur: helloSapeurReducer,
  couvOps: couvOpsReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
