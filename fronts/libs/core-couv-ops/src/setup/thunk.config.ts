import { Action, Store, ThunkAction, ThunkDispatch } from "@reduxjs/toolkit";
import { HttpClient } from "../ports/HttpClient";
import { PubSubClient } from "../ports/PubSubClient";
import { RootState } from "../setup/root.reducer";

export type Dependencies = {
  httpClient: HttpClient;
  pubSubClient: PubSubClient;
};

export type AppThunk = ThunkAction<
  void,
  RootState,
  Dependencies,
  Action<string>
>;

export const testDispatchThunk = async (
  store: Store<RootState>,
  thunk: AppThunk,
) => {
  (store.dispatch as ThunkDispatch<RootState, any, any>)(thunk);
};
