import { AppThunk } from "../../setup/thunk.config";
import { couvOpsActions } from "./couvOps.slice";

export const initiateScheduledEventsThunk = (): AppThunk => async (
  dispatch,
  _,
  { pubSubClient },
) => {
  dispatch(couvOpsActions.scheduledEventsInitiated());
  pubSubClient.send({ topic: "startReplayedEvents", payload: undefined });
};
