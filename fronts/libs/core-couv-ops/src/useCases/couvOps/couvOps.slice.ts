import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { DateString } from "../../utils/dateTypes";
import { CouvOpsUpdatedEventData } from "@fronts/interfaces";

export type VehicleAvailableCount = CouvOpsUpdatedEventData & {
  timestamp: DateString;
};

type CouvOpsState = {
  vehicleAvailableCounts: VehicleAvailableCount[];
  areScheduledEventsPlaying: boolean;
  isReplayedEventBusPreparing: boolean;
};

const initialState: CouvOpsState = {
  vehicleAvailableCounts: [],
  areScheduledEventsPlaying: false,
  isReplayedEventBusPreparing: false,
};

const couvOpsSlice = createSlice({
  name: "couvOps",
  initialState,
  reducers: {
    vehicleAvailabilityChanged: (
      state,
      action: PayloadAction<VehicleAvailableCount>,
    ): CouvOpsState => ({
      ...state,
      vehicleAvailableCounts: [...state.vehicleAvailableCounts, action.payload],
    }),
    scheduledEventsInitiated: (state): CouvOpsState => ({
      ...state,
      areScheduledEventsPlaying: true,
    }),
    setReplayedFromDate: (state): CouvOpsState => ({
      ...state,
      isReplayedEventBusPreparing: true,
    }),
    replayedEventsBusBecameReady: (state): CouvOpsState => ({
      ...state,
      isReplayedEventBusPreparing: false,
    }),
  },
});

export const {
  reducer: couvOpsReducer,
  actions: couvOpsActions,
} = couvOpsSlice;
