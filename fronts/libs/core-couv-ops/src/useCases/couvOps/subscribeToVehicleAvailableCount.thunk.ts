import { AppThunk } from "../../setup/thunk.config";
import { couvOpsActions } from "./couvOps.slice";

export const subscribeToVehicleAvailableCountThunk = (): AppThunk => async (
  dispatch,
  _,
  { pubSubClient },
) => {
  pubSubClient.subscribe("vehicleAvailabilityChanged", (event) => {
    dispatch(
      couvOpsActions.vehicleAvailabilityChanged({
        timestamp: event.timestamp,
        ...event.payload,
      }),
    );
  });
  pubSubClient.subscribe("replayedEventsBusBecameReady", (event) => {
    dispatch(
      couvOpsActions.replayedEventsBusBecameReady(),
    );
  })
};
