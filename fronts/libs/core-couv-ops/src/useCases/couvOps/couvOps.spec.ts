import { Store } from "@reduxjs/toolkit";
import { InMemoryHttpClient } from "../../secondaryAdapters/InMemoryHttpClient";
import { InMemoryPubSubClient } from "../../secondaryAdapters/InMemoryPubSubClient";
import { RootState } from "../../setup/root.reducer";
import { configureReduxStore } from "../../setup/store.config";
import { testDispatchThunk } from "../../setup/thunk.config";
import { DateString } from "../../utils/dateTypes";
import {
  buildExpectStateToEqual,
  ExpectStateToEqual,
} from "../../utils/test.utils";
import { couvOpsActions, VehicleAvailableCount } from "./couvOps.slice";
import { initiateScheduledEventsThunk } from "./initiateScheduledEvents.thunk";
import { setReplayedFromDateThunk } from "./setReplayedFromDate.thunk";

import { subscribeToVehicleAvailableCountThunk } from "./subscribeToVehicleAvailableCount.thunk";

describe("Update couv-ops", () => {
  let store: Store<RootState>;
  let expectStateToEqual: ExpectStateToEqual;
  let httpClient: InMemoryHttpClient;
  let pubSubClient: InMemoryPubSubClient;
  let initialState: RootState;

  beforeEach(() => {
    httpClient = new InMemoryHttpClient();
    pubSubClient = new InMemoryPubSubClient();
    store = configureReduxStore({ httpClient, pubSubClient });
    initialState = store.getState();
    expectStateToEqual = buildExpectStateToEqual(store, store.getState());
  });

  it("Subscribes to vehicles available counts", async () => {
    const timestamp_0 = new Date().toISOString();
    const count_0 = 2;

    await subscribeToVehicleAvailableCount();
    pubSubClient.publish({
      timestamp: timestamp_0,
      topic: "vehicleAvailabilityChanged",
      payload: {
        counts: {
          available: count_0,
          unavailable: 0,
          in_service: 0,
          recoverable: 0,
        },
        role: "victim_rescue",
      },
    });
    const expectedVehiculeAvailableCount_0: VehicleAvailableCount = {
      timestamp: timestamp_0,
      counts: {
        available: count_0,
        unavailable: 0,
        in_service: 0,
        recoverable: 0,
      },
      role: "victim_rescue",
    };

    expectStateToEqual({
      couvOps: {
        vehicleAvailableCounts: [expectedVehiculeAvailableCount_0],
        areScheduledEventsPlaying: false,
        isReplayedEventBusPreparing: false,
      },
    });
  });

  it("starts the prepared event list", async () => {
    await initiateScheduledEvents();
    expectStateToEqual({
      couvOps: {
        ...initialState.couvOps,
        areScheduledEventsPlaying: true,
      },
    });
    expect(pubSubClient.commandsSent).toEqual([
      {
        topic: "startReplayedEvents",
      },
    ]);
  });

  describe("Replayed Event Bus actions", () => {
    it("Triggers preparation of replayed event bus", async () => {
      const dateString = new Date().toISOString();
      await setReplayedFromDate(dateString);
      expectStateToEqual({
        couvOps: {
          ...initialState.couvOps,
          isReplayedEventBusPreparing: true,
        },
      });
      expect(pubSubClient.commandsSent).toEqual([
        {
          topic: "setReplayedFromDate",
          payload: dateString,
        },
      ]);
    });

    it("stops showing event bus is preparing", () => {
      store.dispatch(couvOpsActions.setReplayedFromDate());
      expectStateToEqual({
        couvOps: {
          ...initialState.couvOps,
          isReplayedEventBusPreparing: true,
        },
      });
      store.dispatch(couvOpsActions.replayedEventsBusBecameReady());
      expectStateToEqual({
        couvOps: {
          ...initialState.couvOps,
          isReplayedEventBusPreparing: false,
        },
      });
    });
  });

  const subscribeToVehicleAvailableCount = () =>
    testDispatchThunk(store, subscribeToVehicleAvailableCountThunk());

  const initiateScheduledEvents = () =>
    testDispatchThunk(store, initiateScheduledEventsThunk());

  const setReplayedFromDate = (date: DateString) =>
    testDispatchThunk(store, setReplayedFromDateThunk(date));
});
