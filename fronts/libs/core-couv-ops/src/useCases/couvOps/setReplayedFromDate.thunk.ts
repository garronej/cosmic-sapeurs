import { AppThunk } from "../../setup/thunk.config";
import { DateString } from '../../utils/dateTypes';
import { couvOpsActions } from "./couvOps.slice";

export const setReplayedFromDateThunk = (date: DateString): AppThunk => async (
  dispatch,
  _,
  { pubSubClient },
) => {
  dispatch(couvOpsActions.setReplayedFromDate());
  pubSubClient.send({ topic: "setReplayedFromDate", payload: date });
};