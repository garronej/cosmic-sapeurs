import { Store } from "@reduxjs/toolkit";
import { RootState } from "../setup/root.reducer";

export const buildExpectStateToEqual = (
  store: Store<RootState>,
  initialState: RootState,
) => (expectedState: Partial<RootState>) => {
  expect(store.getState()).toEqual({ ...initialState, ...expectedState });
};

export type ExpectStateToEqual = ReturnType<typeof buildExpectStateToEqual>;
