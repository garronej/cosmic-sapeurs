import { CouvOpsUpdatedEventData } from "@fronts/interfaces";
import { DateString } from "../utils/dateTypes";

type VehicleAvailabilityChangedPayload = CouvOpsUpdatedEventData;

type WithTopicAndPayload<T extends string, P> = {
  topic: T;
  payload: P;
};

type Event<T extends string, P> = WithTopicAndPayload<T, P> & {
  timestamp: DateString;
};

export type AppEvent =
  | Event<"vehicleAvailabilityChanged", VehicleAvailabilityChangedPayload>
  | Event<"replayedEventsBusBecameReady", void>;

type Command<T extends string, P = void> = WithTopicAndPayload<T, P>;

export type AppCommand =
  | Command<"startReplayedEvents">
  | Command<"setReplayedFromDate", DateString>;

export type NarrowEvent<T extends AppEvent["topic"]> = Extract<
  AppEvent,
  { topic: T }
>;

export interface PubSubClient {
  subscribe: <T extends AppEvent["topic"]>(
    topic: T,
    callback: (event: NarrowEvent<T>) => void,
  ) => void;
  send: (command: AppCommand) => void;
}
