import {
  AppCommand,
  AppEvent,
  NarrowEvent,
  PubSubClient,
} from "../ports/PubSubClient";
import { v4 as uuid4 } from "uuid";

export class WebSocketPubSubClient implements PubSubClient {
  private socket?: WebSocket;

  constructor(private socketUrl: string) {}

  subscribe<T extends AppEvent["topic"]>(
    topic: T,
    callback: (event: NarrowEvent<T>) => void,
  ): void {
    if (!this.socket) {
      this.socket = new WebSocket(`${this.socketUrl}`);
      this.socket.addEventListener("open", (event) => {
        console.log("Websocket open", event);
        // const appEvent: AppEvent = {
        //   topic: "vehicleAvailabilityChanged",
        //   timestamp: new Date().toISOString(),
        //   payload: {
        //     count: 12,
        //   },
        // };
        // setTimeout(() => this.socket.send(JSON.stringify(appEvent)), 2000);
      });
    }
    this.socket.addEventListener("message", (event) => {
      console.log("Websocket on message", event);
      const eventData = JSON.parse(event.data);
      console.log({ eventData, topic });
      if (eventData.topic == topic) callback(eventData);
    });
  }

  send(command: AppCommand) {
    if (!this.socket) throw new Error("No websocket connections open");
    this.socket.send(JSON.stringify(command));
  }
}
