import {
  AppCommand,
  AppEvent,
  NarrowEvent,
  PubSubClient,
} from "../ports/PubSubClient";

type Subscriptions = {
  [key in AppEvent["topic"]]: Array<(event: AppEvent) => void>;
};

export class InMemoryPubSubClient implements PubSubClient {
  private subscriptions: Subscriptions = {
    vehicleAvailabilityChanged: [],
    replayedEventsBusBecameReady: [],
  };

  private _commandsSent: AppCommand[] = [];

  subscribe<T extends AppEvent["topic"]>(
    topic: T,
    callback: (narrowEvent: NarrowEvent<T>) => void,
  ): void {
    this.subscriptions[topic].push(callback as (event: AppEvent) => void);
    // setInterval(
    //   () =>
    //     this.publish({
    //       timestamp: new Date().toISOString(),
    //       topic: "vehicleAvailabilityChanged",
    //       payload: {
    //         count: 8,
    //       },
    //     }),
    //   2000,
    // );
  }

  publish(event: AppEvent) {
    this.subscriptions[event.topic].forEach((callback) => {
      callback(event);
    });
  }

  send(command: AppCommand) {
    this._commandsSent.push(command);
  }

  get commandsSent() {
    return this._commandsSent;
  }
}
