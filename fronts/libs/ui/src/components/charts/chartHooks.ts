import { useRef, useState } from "react";

export const useChart = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [chart, setChart] = useState<Chart>();
  return {
    chart,
    setChart,
    canvasRef,
  };
};
