import { Meta, Story } from "@storybook/react/types-6-0";
import * as React from "react";
import { useEffect, useState } from "react";
import { LineChartProps } from "./lineChartHooks";
import { LineInputSample, LineVariableToStyle } from "./lineChartUtils";
import { StaticLineChart } from "./StaticLineChart";
import { StreamingLineChart } from "./StreamingLineChart";

const Template: Story<LineChartProps<VehicleRole>> = (args) => (
  <StaticLineChart {...args} />
);
type VehicleRole = "vSAV Count" | "pump Count" | "other Count";

const variableToStyle: LineVariableToStyle<VehicleRole> = {
  "vSAV Count": {
    color: "red",
    label: "Available victim vehicle",
  },
  "pump Count": {
    color: "blue",
    label: "Available pump vehicle",
  },
};

const props: LineChartProps<VehicleRole> = {
  data: [
    { timestamp: "2020-01-01T01", value: 1, variable: "vSAV Count" },
    { timestamp: "2020-01-02T01", value: null, variable: "vSAV Count" },
    { timestamp: "2020-01-03T01", value: 10, variable: "vSAV Count" },
    { timestamp: "2020-01-03T02", value: 90, variable: "pump Count" },
    { timestamp: "2020-01-03T03", value: 13, variable: "pump Count" },
    { timestamp: "2020-01-03T06", value: 19, variable: "pump Count" },
  ],
  tMin: "2020-01-01T01",
  tMax: "2020-01-03T06",
  variableToStyle,
};

export const LineChartStory = Template.bind({});
LineChartStory.args = props;

const TemplateStream: Story<LineChartProps<VehicleRole>> = () => {
  const [data, setData] = useState<LineInputSample<VehicleRole>[]>([]);
  const variableOptions: VehicleRole[] = [
    "pump Count",
    "vSAV Count",
    "other Count",
  ];

  useEffect(() => {
    setTimeout(() => {
      setData([
        ...data,
        {
          timestamp: new Date().toISOString(),
          value: Math.floor(Math.random() * 100),
          variable:
            variableOptions[Math.floor(Math.random() * variableOptions.length)],
        },
      ]);
    }, 500);
  }, [data]);

  return (
    <div>
      <StreamingLineChart
        data={data}
        tSlice={100}
        tUnit={"second"}
        variableToStyle={variableToStyle}
      />
    </div>
  );
};
export const StreamingLineChartStory = TemplateStream.bind({});

export default {
  title: "Line Charts",
  component: StreamingLineChartStory,
  argTypes: {},
} as Meta;
