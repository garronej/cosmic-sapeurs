import { TimeUnit } from "chart.js";
import * as React from "react";
import { useEffect } from "react";
import { LineChartProps, useLineChartJS } from "./lineChartHooks";
import {
  inputDataToChartJSData,
  LineInputSample,
  LineVariableToStyle,
} from "./lineChartUtils";
import { subSeconds } from "date-fns";

export type StreamingLineChartProps<T extends string> = {
  data: LineInputSample<T>[];
  tUnit?: TimeUnit;
  tSlice: number;
  yMin?: number;
  yMax?: number;
  variableToStyle?: LineVariableToStyle<T>;
  tMax?: Date;
};

export const StreamingLineChart = <T extends string>(
  props: StreamingLineChartProps<T>,
) => {
  const tMax = props.tMax;

  const initStaticLineChartProps: LineChartProps<T> = {
    ...props,
    tMin: tMax && subSeconds(tMax, props.tSlice).toISOString(),
    tMax: tMax && tMax.toISOString(),
  };
  const { canvasRef, chart } = useLineChartJS(initStaticLineChartProps);

  useEffect(() => {
    if (!chart) return;
    chart.data = inputDataToChartJSData(props.data, props.variableToStyle);
    if (tMax) {
      chart.options.scales!.xAxes![0].time!.min = subSeconds(
        tMax,
        props.tSlice,
      ).toISOString();
      chart.options.scales!.xAxes![0].time!.max = tMax.toISOString();
    }
    chart.update();
  }, [props.data]);

  return (
    <div style={{ width: "500px" }}>
      <canvas ref={canvasRef} />
    </div>
  );
};
