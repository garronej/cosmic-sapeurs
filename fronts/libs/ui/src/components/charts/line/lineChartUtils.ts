import { replaceAt, replaceLast } from "@fronts/utilities";
import { ChartDataSets } from "chart.js";
import * as R from "ramda";

export type ChartJSDataset<V extends string> = ChartDataSets & {
  variable: V;
  data: (number | null)[];
};

export type ChartJSData<V extends string> = {
  labels: string[];
  datasets: ChartJSDataset<V>[];
};

export type LineInputSample<T extends string> = {
  timestamp: string; // Should be DateString
  value: number | null;
  variable: T;
};

export type LineVariableToStyle<T extends string> = Partial<
  {
    [key in T]: {
      color: string;
      label: string;
    };
  }
>;

export const inputDataToChartJSData = <T extends string>(
  inputs: LineInputSample<T>[],
  variableToStyle?: LineVariableToStyle<T>,
): ChartJSData<T> => {
  const initial: ChartJSData<T> = { datasets: [], labels: [] };
  const sortedInputs = R.sortBy<LineInputSample<T>>(
    (input) => new Date(input.timestamp).getTime(),
    inputs,
  );

  return sortedInputs.reduce((acc, input) => {
    const isTimestampNew = !acc.labels.includes(input.timestamp);
    const initialDatasetIndex = acc.datasets.findIndex(
      (dataset) => dataset.variable === input.variable,
    );

    // we add the dataset with the right label if it does not exist
    const withVariableDatasets: ChartJSDataset<T>[] =
      initialDatasetIndex === -1
        ? [
            ...acc.datasets,
            {
              variable: input.variable,
              label: variableToStyle && variableToStyle[input.variable]?.label,
              data: Array(Math.max(0, acc.labels.length)).fill(null),
              borderColor:
                variableToStyle && variableToStyle[input.variable]?.color,
              fill: false,
            },
          ]
        : acc.datasets;

    // now we are sure to find a dataset with the variable as label
    const datasetIndex = withVariableDatasets.findIndex(
      (dataset) => dataset.variable === input.variable,
    )!;

    // if timestamp is new, push last value in all datasets' data
    const datasetsWithNull: ChartJSDataset<T>[] = isTimestampNew
      ? withVariableDatasets.map((dataset) => {
          return {
            ...dataset,
            data: [
              ...dataset.data,
              dataset.data.length
                ? dataset.data[dataset.data.length - 1]
                : null,
            ],
          };
        })
      : withVariableDatasets;

    const datasetWithNull = datasetsWithNull[datasetIndex];

    // replace last null with actual value in correct dataset
    const datasets: ChartJSDataset<T>[] = replaceAt(
      datasetsWithNull,
      datasetIndex,
      {
        ...datasetWithNull,
        data: replaceLast(datasetWithNull.data, input.value),
      },
    );

    return {
      ...acc,
      datasets,
      labels: isTimestampNew ? [...acc.labels, input.timestamp] : acc.labels,
    };
  }, initial);
};
