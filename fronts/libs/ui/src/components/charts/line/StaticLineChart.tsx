import * as React from "react";
import { LineChartProps, useLineChartJS } from "./lineChartHooks";

export const StaticLineChart = <T extends string>(props: LineChartProps<T>) => {
  const { canvasRef } = useLineChartJS(props);
  return (
    <div style={{ border: "1px solid green", maxWidth: "600px" }}>
      <h1>Static Line Chart </h1>
      <canvas ref={canvasRef} />
    </div>
  );
};
