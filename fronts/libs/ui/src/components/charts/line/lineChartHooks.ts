import Chart, { TimeUnit } from "chart.js";
import { useRef, useState, useEffect } from "react";
import { useChart } from "../chartHooks";
import {
  LineInputSample,
  inputDataToChartJSData,
  LineVariableToStyle,
} from "./lineChartUtils";

export type LineChartProps<T extends string> = {
  data: LineInputSample<T>[];
  tUnit?: TimeUnit;
  tMin?: string;
  tMax?: string;
  yMin?: number;
  yMax?: number;
  variableToStyle?: LineVariableToStyle<T>;
};

export const useLineChartJS = <T extends string>({
  data,
  tUnit,
  tMin,
  tMax,
  yMin = 0,
  yMax,
  variableToStyle,
}: LineChartProps<T>) => {
  const { chart, setChart, canvasRef } = useChart();
  useEffect(() => {
    if (canvasRef && canvasRef.current) {
      const newChart = new Chart(canvasRef.current, {
        type: "line",
        data: inputDataToChartJSData(data, variableToStyle),
        options: {
          legend: { display: false },
          animation: { duration: 0 },
          spanGaps: true, // draw lines between points with no or null data
          elements: {
            point: {
              radius: 0,
            },
          },
          scales: {
            xAxes: [
              {
                type: "time",
                time: {
                  unit: tUnit,
                  min: tMin,
                  max: tMax,
                  displayFormats: {
                    hour: "HH:mm",
                  },
                },
                ticks: {
                  autoSkip: false,
                  maxRotation: 0,
                  minRotation: 0,
                },
              },
            ],
            yAxes: [
              {
                ticks: {
                  max: yMax,
                  min: yMin,
                },
              },
            ],
          },
        },
      });
      setChart(newChart);
    }
  }, [canvasRef]);
  return { canvasRef, chart };
};
