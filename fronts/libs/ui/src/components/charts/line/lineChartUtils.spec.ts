import {
  LineInputSample,
  inputDataToChartJSData,
  LineVariableToStyle,
} from "./lineChartUtils";
import { ChartJSData } from "../../chartUtils";

type Variables = "vSAVCount" | "pumpCount" | "otherCount";

const variableToStyle: LineVariableToStyle<Variables> = {
  vSAVCount: {
    color: "red",
    label: "Available victim vehicle",
  },
  pumpCount: {
    color: "blue",
    label: "Available pump vehicle",
  },
  otherCount: {
    color: "green",
    label: "Available other vehicle",
  },
};

describe("Line Chart Utils", () => {
  describe("Convert input data sample to Chart.js data", () => {
    it("Returns empty datasets, when no data", () => {
      expectConvertedDataToBe(inputDataToChartJSData([], {}), {
        datasets: [],
        labels: [],
      });
    });
    it("Returns correct format, when one data sample is given and no style", () => {
      const timetamp0 = "2020-10-21T15:10:59.024Z";
      const inputSampleVSAV: LineInputSample<Variables> = {
        timestamp: timetamp0,
        value: 12,
        variable: "vSAVCount",
      };
      expectConvertedDataToBe(inputDataToChartJSData([inputSampleVSAV]), {
        datasets: [
          {
            data: [12],
            label: undefined,
            borderColor: undefined,
            variable: "vSAVCount",
          },
        ],
        labels: [timetamp0],
      });
    });
    it("Returns correct format, with multiple datasets and shuffled timestamps", () => {
      const timetamp0 = "2020-10-21T15:10:59.024Z";
      const timetamp1 = "2020-10-21T16:10:59.024Z";

      expectConvertedDataToBe(
        inputDataToChartJSData(
          [
            { timestamp: timetamp0, value: 12, variable: "vSAVCount" },
            { timestamp: timetamp0, value: 9, variable: "pumpCount" },
            { timestamp: timetamp1, value: 14, variable: "vSAVCount" },
            { timestamp: timetamp1, value: 10, variable: "pumpCount" },
          ],
          variableToStyle,
        ),
        {
          datasets: [
            {
              data: [12, 14],
              variable: "vSAVCount",
              label: "Available victim vehicle",
              borderColor: "red",
            },
            {
              data: [9, 10],
              variable: "pumpCount",
              label: "Available pump vehicle",
              borderColor: "blue",
            },
          ],
          labels: [timetamp0, timetamp1],
        },
      );
    });
    it("Returns correct format, with mixed ", () => {
      const timetamp0 = "2020-10-21T15:10:59.024Z";
      const timetamp1 = "2020-10-21T16:10:59.024Z";

      expectConvertedDataToBe(
        inputDataToChartJSData(
          [
            { timestamp: timetamp0, value: 12, variable: "vSAVCount" },
            { timestamp: timetamp1, value: 10, variable: "pumpCount" },
            { timestamp: timetamp0, value: 9, variable: "pumpCount" },
          ],
          variableToStyle,
        ),
        {
          datasets: [
            {
              data: [12, 12],
              variable: "vSAVCount",
              label: "Available victim vehicle",
              borderColor: "red",
            },
            {
              data: [9, 10],
              variable: "pumpCount",
              label: "Available pump vehicle",
              borderColor: "blue",
            },
          ],
          labels: [timetamp0, timetamp1],
        },
      );
    });
    it("Returns correct format, with disjoint timestamp dataset", () => {
      const inputData: LineInputSample<Variables>[] = [
        { timestamp: "2020-01-01T01", value: 1, variable: "vSAVCount" },
        { timestamp: "2020-01-02T01", value: 9, variable: "vSAVCount" },
        { timestamp: "2020-01-03T01", value: 10, variable: "vSAVCount" },
        { timestamp: "2020-01-03T02", value: 90, variable: "pumpCount" },
        { timestamp: "2020-01-03T03", value: 13, variable: "pumpCount" },
        { timestamp: "2020-01-03T06", value: 19, variable: "pumpCount" },
      ];

      expectConvertedDataToBe(
        inputDataToChartJSData(inputData, variableToStyle),
        {
          datasets: [
            {
              data: [1, 9, 10, 10, 10, 10],
              variable: "vSAVCount",
              label: "Available victim vehicle",
              borderColor: "red",
            },
            {
              data: [null, null, null, 90, 13, 19],
              variable: "pumpCount",
              label: "Available pump vehicle",
              borderColor: "blue",
            },
          ],
          labels: [
            "2020-01-01T01",
            "2020-01-02T01",
            "2020-01-03T01",
            "2020-01-03T02",
            "2020-01-03T03",
            "2020-01-03T06",
          ],
        },
      );
    });
  });
  const expectConvertedDataToBe = (
    actual: ChartJSData<Variables>,
    expected: ChartJSData<Variables>,
  ) => {
    expect(actual).toMatchObject(expected);
  };
});
