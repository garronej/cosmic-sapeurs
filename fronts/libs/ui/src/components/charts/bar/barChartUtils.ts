import { ChartDataSets } from "chart.js";

export type BarInputSample<Role extends string, CountKind extends string> = {
  counts: Partial<
    {
      [key in CountKind]: number;
    }
  >;
  role: Role;
};

export type BarVariableToStyle<
  Role extends string,
  CountKind extends string
> = {
  [key in Role]: {
    colors: {
      [key in CountKind]: string;
    };
    label: string;
  };
};

export type BarChartJSData<L extends string, V extends string> = {
  roles: L[];
  labels?: string[];
  datasets: Array<ChartDataSets & { variable: V; data: (number | null)[] }>;
};

type InputDataToBarChartJSDataProps<
  Role extends string,
  CountKind extends string
> = {
  inputSamples: BarInputSample<Role, CountKind>[];
  roleOptions: Role[];
  countKindOptions: CountKind[];
  variableToStyle?: BarVariableToStyle<Role, CountKind>;
};

export const inputDataToBarChartJSData = <
  Role extends string,
  CountKind extends string
>({
  countKindOptions,
  inputSamples,
  roleOptions,
  variableToStyle,
}: InputDataToBarChartJSDataProps<Role, CountKind>): BarChartJSData<
  Role,
  CountKind
> => {
  return {
    datasets: countKindOptions.map((countKind) => ({
      variable: countKind,
      data: roleOptions.map((role) => {
        const matchInputs = inputSamples.filter((input) => input.role === role);
        const lastMatchInput = matchInputs.pop();
        return lastMatchInput?.counts[countKind] ?? null;
      }),
      backgroundColor:
        variableToStyle &&
        roleOptions.map((role) => variableToStyle[role].colors[countKind]),
    })),

    roles: roleOptions,
    labels:
      variableToStyle && roleOptions.map((role) => variableToStyle[role].label),
  };
};
