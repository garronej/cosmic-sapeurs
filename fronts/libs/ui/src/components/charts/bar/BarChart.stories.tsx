import React from "react";
import { Meta, Story } from "@storybook/react/types-6-0";
import { BarChart, BarChartProps } from "./BarChart";
import { BarInputSample, BarVariableToStyle } from "./barChartUtils";
import { VehicleRole, VehicleAvailablilityKind } from "@fronts/interfaces";

const roleOptions: VehicleRole[] = ["victim_rescue", "pump", "other"];

const Template: Story<BarChartProps<VehicleRole, VehicleAvailablilityKind>> = (
  args,
) => <BarChart {...args} />;
export const BarChartStory = Template.bind({});

const countKindOptions: VehicleAvailablilityKind[] = [
  "available",
  "in_service",
  "unavailable",
  "recoverable",
];
const inputSamples: BarInputSample<VehicleRole, VehicleAvailablilityKind>[] = [
  {
    counts: {
      available: 10,
      unavailable: 3,
      recoverable: 4,
      in_service: 90,
    },
    role: "victim_rescue",
  },
  {
    counts: {
      available: 10,
      unavailable: 2,
      recoverable: 9,
      in_service: 3,
    },
    role: "pump",
  },
  {
    counts: {
      available: 100,
      unavailable: 2,
      recoverable: 9,
      in_service: 20,
    },
    role: "other",
  },
];

const commonColors = {
  unavailable: "lightgray",
  recoverable: "gray",
};
const barVariableToStyle: BarVariableToStyle<
  VehicleRole,
  VehicleAvailablilityKind
> = {
  victim_rescue: {
    label: "Victim Rescue",
    colors: {
      ...commonColors,
      available: "blue",
      in_service: "lightblue",
    },
  },
  pump: {
    label: "Pump",
    colors: {
      ...commonColors,
      available: "green",
      in_service: "lightgreen",
    },
  },
  other: {
    label: "Other",
    colors: {
      ...commonColors,
      available: "red",
      in_service: "pink",
    },
  },
};

const props: BarChartProps<VehicleRole, VehicleAvailablilityKind> = {
  inputSamples,
  barVariableToStyle,
  countKindOptions,
  roleOptions,
};

BarChartStory.args = props;

export default {
  title: "Bar Chart",
  component: BarChartStory,
} as Meta;
