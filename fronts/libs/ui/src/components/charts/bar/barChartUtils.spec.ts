import {
  BarChartJSData,
  BarVariableToStyle,
  inputDataToBarChartJSData,
} from "./barChartUtils";
import { VehicleRole, VehicleAvailablilityKind } from "@fronts/interfaces";

const roleOptions: VehicleRole[] = ["victim_rescue", "pump", "other"];

const countKindOptions: VehicleAvailablilityKind[] = [
  "available",
  "unavailable",
  "recoverable",
  "in_service",
];

const commonColors = {
  unavailable: "lightgray",
  recoverable: "gray",
};
const variableToStyle: BarVariableToStyle<
  VehicleRole,
  VehicleAvailablilityKind
> = {
  victim_rescue: {
    label: "Victim Rescue",
    colors: {
      ...commonColors,
      available: "blue",
      in_service: "lightblue",
    },
  },
  pump: {
    label: "Pump",
    colors: {
      ...commonColors,
      available: "green",
      in_service: "lightgreen",
    },
  },
  other: {
    label: "Other",
    colors: {
      ...commonColors,
      available: "red",
      in_service: "lightred",
    },
  },
};
describe("Bar Chart Utils", () => {
  describe("Convert input data sample to Chart.js data", () => {
    it("Returns empty datasets, when no data and empty role and count options", () => {
      expectConvertedDataToBe(
        inputDataToBarChartJSData({
          inputSamples: [],
          roleOptions: [],
          countKindOptions: [],
        }),
        {
          datasets: [],
          roles: [],
        },
      );
    });
    it("Returns empty datasets, when no data but role and count options", () => {
      expectConvertedDataToBe(
        inputDataToBarChartJSData({
          inputSamples: [],
          roleOptions,
          countKindOptions,
        }),
        {
          datasets: [
            { variable: "available", data: [null, null, null] },
            { variable: "unavailable", data: [null, null, null] },
            { variable: "recoverable", data: [null, null, null] },
            { variable: "in_service", data: [null, null, null] },
          ],
          roles: ["victim_rescue", "pump", "other"],
        },
      );
    });

    it("Returns correct format, when one data sample is given with colors", () => {
      expectConvertedDataToBe(
        inputDataToBarChartJSData({
          inputSamples: [
            {
              counts: {
                available: 10,
                unavailable: 3,
                recoverable: 4,
                in_service: 90,
              },
              role: "victim_rescue",
            },
          ],
          roleOptions,
          countKindOptions,
          variableToStyle,
        }),
        {
          datasets: [
            {
              variable: "available",
              data: [10, null, null],
              backgroundColor: ["blue", "green", "red"],
            },
            {
              variable: "unavailable",
              data: [3, null, null],
              backgroundColor: ["lightgray", "lightgray", "lightgray"],
            },
            {
              variable: "recoverable",
              data: [4, null, null],
              backgroundColor: ["gray", "gray", "gray"],
            },
            {
              variable: "in_service",
              data: [90, null, null],
              backgroundColor: ["lightblue", "lightgreen", "lightred"],
            },
          ],
          labels: ["Victim Rescue", "Pump", "Other"],
          roles: ["victim_rescue", "pump", "other"],
        },
      );
    });
    it("Returns correct format, when multiple data sample are given with colors", () => {
      expectConvertedDataToBe(
        inputDataToBarChartJSData({
          inputSamples: [
            {
              counts: {
                available: 10,
                unavailable: 3,
                recoverable: 4,
                in_service: 90,
              },
              role: "victim_rescue",
            },
            {
              counts: {
                available: 10,
                unavailable: 2,
                recoverable: 9,
                in_service: 0,
              },
              role: "pump",
            },
            {
              counts: {
                available: 22,
                unavailable: 2,
                recoverable: 9,
                in_service: 0,
              },
              role: "pump",
            },
            {
              counts: {
                available: 1,
                unavailable: 2,
                recoverable: 3,
                in_service: null,
              },
              role: "other",
            },
          ],
          roleOptions,
          countKindOptions,
          variableToStyle,
        }),
        {
          datasets: [
            {
              variable: "available",
              data: [10, 22, 1],
              backgroundColor: ["blue", "green", "red"],
            },
            {
              variable: "unavailable",
              data: [3, 2, 2],
              backgroundColor: ["lightgray", "lightgray", "lightgray"],
            },
            {
              variable: "recoverable",
              data: [4, 9, 3],
              backgroundColor: ["gray", "gray", "gray"],
            },
            {
              variable: "in_service",
              data: [90, 0, null],
              backgroundColor: ["lightblue", "lightgreen", "lightred"],
            },
          ],
          roles: ["victim_rescue", "pump", "other"],
          labels: ["Victim Rescue", "Pump", "Other"],
        },
      );
    });
  });
  const expectConvertedDataToBe = (
    actual: BarChartJSData<VehicleRole, VehicleAvailablilityKind>,
    expected: BarChartJSData<VehicleRole, VehicleAvailablilityKind>,
  ) => {
    expect(actual).toMatchObject(expected);
  };
});
