import Chart from "chart.js";
import React, { useEffect } from "react";
import { useChart } from "../chartHooks";
import {
  BarInputSample,
  BarVariableToStyle,
  inputDataToBarChartJSData,
} from "./barChartUtils";

export type BarChartProps<Role extends string, CountKind extends string> = {
  inputSamples: BarInputSample<Role, CountKind>[];
  roleOptions: Role[];
  countKindOptions: CountKind[];
  barVariableToStyle?: BarVariableToStyle<Role, CountKind>;
  yMax?: number;
};

const useBarChart = <Role extends string, CountKind extends string>({
  inputSamples,
  roleOptions,
  countKindOptions,
  barVariableToStyle,
  yMax,
}: BarChartProps<Role, CountKind>) => {
  const { setChart, canvasRef } = useChart();
  const chartJSData = inputDataToBarChartJSData({
    inputSamples,
    roleOptions,
    countKindOptions,
    variableToStyle: barVariableToStyle,
  });

  useEffect(() => {
    if (canvasRef?.current) {
      const newChart = new Chart(canvasRef.current, {
        type: "bar",
        data: chartJSData,
        options: {
          animation: { duration: 0 },
          legend: { display: false },
          tooltips: {
            mode: "index",
            intersect: false,
          },
          responsive: true,
          scales: {
            xAxes: [
              {
                stacked: true,
              },
            ],
            yAxes: [
              {
                ticks: { max: yMax },
                stacked: true,
              },
            ],
          },
        },
      });
      setChart(newChart);
    }
  }, [inputSamples]);

  return { canvasRef };
};

export const BarChart = <Role extends string, CountKind extends string>(
  props: BarChartProps<Role, CountKind>,
) => {
  const { canvasRef } = useBarChart(props);

  return (
    <div style={{ width: "500px" }}>
      <canvas ref={canvasRef} />
    </div>
  );
};
