import { ChartDataSets } from "chart.js";

export type ChartJSDataset<T extends string> = ChartDataSets & {
  variable: T;
  data: (number | null)[];
};

export type ChartJSData<T extends string> = {
  labels: string[];
  datasets: ChartJSDataset<T>[];
};
