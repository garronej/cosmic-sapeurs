import React from "react";

export type WelcomeUiProps = {
  color?: string;
  children: string;
};

export const WelcomeUi = ({ color = "red", children }: WelcomeUiProps) => {
  return (
    <div style={{ color }}>
      <h1>{children}</h1>
    </div>
  );
};
