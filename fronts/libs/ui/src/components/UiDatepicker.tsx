import TextField from "@material-ui/core/TextField";
import * as React from "react";

type UiDatepickerProps = {
  value?: string;
  onChange: (
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => void;
};

export const UiDatepicker = ({ value, onChange }: UiDatepickerProps) => {
  return (
    <TextField
      id="date"
      variant="outlined"
      label="Début"
      type="date"
      defaultValue="2020-10-04"
      value={value}
      onChange={onChange}
    />
  );
};
