import React from "react";
import { Meta, Story } from "@storybook/react/types-6-0";
import { WelcomeUi, WelcomeUiProps } from "./WelcomeUi";

const Template: Story<WelcomeUiProps> = (args) => <WelcomeUi {...args} />;
export const WelcomeUiStory = Template.bind({});

const props: WelcomeUiProps = {
  color: "#e414E4",
  children: "Yolo !",
};

WelcomeUiStory.args = props;

export default {
  title: "WelcomeUi",
  component: WelcomeUiStory,
  argTypes: {
    color: { control: "color" },
  },
} as Meta;
