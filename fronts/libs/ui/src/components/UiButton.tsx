import * as React from "react";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";

type UiButtonProps = {
  onClick: () => void;
  children: React.ReactNode;
  disabled?: boolean;
  isLoading?: boolean;
};

const useStyles = makeStyles(() => ({
  button: {
    margin: "1rem",
    width: "270px",
  },
}));

export const UiButton = ({
  onClick,
  children,
  disabled,
  isLoading,
}: UiButtonProps) => {
  const classes = useStyles();

  return (
    <Button
      onClick={onClick}
      disabled={disabled ?? isLoading}
      color="primary"
      variant="contained"
      className={classes.button}
      endIcon={isLoading && <CircularProgress color="inherit" size={20} />}
    >
      {children}
    </Button>
  );
};
