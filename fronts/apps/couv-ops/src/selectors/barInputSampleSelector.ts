import { RootState, VehicleAvailableCount } from "@fronts/core-couv-ops";
import {
  VehicleRole,
  CouvOpsUpdatedEventData,
  VehicleAvailablilityKind,
} from "@fronts/interfaces";
import { LineInputSample, BarInputSample } from "@fronts/ui";
import * as R from "ramda";

export const barInputSampleSelector = (
  state: RootState,
): BarInputSample<VehicleRole, VehicleAvailablilityKind>[] => {
  const countsByRole = R.groupBy<VehicleAvailableCount>(
    (count) => count.role,
    state.couvOps.vehicleAvailableCounts,
  );

  const lastCountByRole: {
    [k in VehicleRole]: CouvOpsUpdatedEventData;
  } = R.map<any, any>(
    (values) =>
      R.sortBy<LineInputSample<VehicleRole>>(
        (value) => new Date(value.timestamp).getTime(),
        values,
      ).pop(),
    countsByRole,
  );

  const barInputSamples: BarInputSample<
    VehicleRole,
    VehicleAvailablilityKind
  >[] = (Object.keys(lastCountByRole) as VehicleRole[]).map((role) => ({
    counts: lastCountByRole[role].counts,
    role: role,
  }));
  return barInputSamples;
};
