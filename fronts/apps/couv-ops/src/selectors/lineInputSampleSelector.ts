import { RootState } from '@fronts/core-couv-ops';
import { VehicleRole } from '@fronts/interfaces';
import { LineInputSample } from '@fronts/ui';


export const lineInputSampleSelector = (state: RootState) =>
    state.couvOps.vehicleAvailableCounts.map<LineInputSample<VehicleRole>>(
      ({ role, counts, timestamp }) => ({
        timestamp,
        value: counts.available ?? null,
        variable: role,
      }),
    )