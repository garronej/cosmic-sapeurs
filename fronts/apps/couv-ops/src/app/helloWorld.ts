import { RootState } from "@fronts/core-couv-ops";
import { useSelector } from "react-redux";

export const useHelloWorld = () => {
  const message = useSelector((state: RootState) => state.helloSapeur.message);
  const isFetching = useSelector(
    (state: RootState) => state.helloSapeur.isFetching,
  );
  return { message, isFetching };
};
