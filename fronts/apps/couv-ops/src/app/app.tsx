import { actions, RootState } from "@fronts/core-couv-ops";
import { VehicleAvailablilityKind, VehicleRole } from "@fronts/interfaces";
import {
  BarChart,
  BarVariableToStyle,
  LineVariableToStyle,
  StreamingLineChart,
  UiButton,
  UiDatepicker,
} from "@fronts/ui";
import { max } from "date-fns";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { barInputSampleSelector } from "../selectors/barInputSampleSelector";
import { lineInputSampleSelector } from "../selectors/lineInputSampleSelector";
import "./app.css";
import { useHelloWorld } from "./helloWorld";

const roleOptions: VehicleRole[] = ["victim_rescue", "pump", "other"];
const countKindOptions: VehicleAvailablilityKind[] = [
  "available",
  "in_service",
  "recoverable",
  "unavailable",
];

const lineVariableToStyle: LineVariableToStyle<VehicleRole> = {
  victim_rescue: {
    color: "red",
    label: "Available victim vehicle",
  },
  pump: {
    color: "blue",
    label: "Available pump vehicle",
  },
  other: {
    color: "green",
    label: "Available other vehicle",
  },
};

const commonColors = {
  unavailable: "lightgray",
  recoverable: "gray",
};
const barVariableToStyle: BarVariableToStyle<
  VehicleRole,
  VehicleAvailablilityKind
> = {
  victim_rescue: {
    label: "VSAV",
    colors: {
      ...commonColors,
      available: "red",
      in_service: "pink",
    },
  },
  pump: {
    label: "EP",
    colors: {
      ...commonColors,
      available: "blue",
      in_service: "lightblue",
    },
  },
  other: {
    label: "Autres",
    colors: {
      ...commonColors,
      available: "green",
      in_service: "lightgreen",
    },
  },
};

export const App = () => {
  const { message, isFetching } = useHelloWorld();
  const areScheduledEventsPlaying = useSelector(
    (state: RootState) => state.couvOps.areScheduledEventsPlaying,
  );
  const isReplayedEventBusPreparing = useSelector(
    (state: RootState) => state.couvOps.isReplayedEventBusPreparing,
  );
  const vehicleAvailableCounts = useSelector(lineInputSampleSelector);
  const vehicleStatusInputSamples = useSelector(barInputSampleSelector);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(actions.subscribeToVehicleAvailableCountThunk());
  }, []);

  const [eventStartDate, setEventStartDate] = React.useState<string>(
    "2020-10-01",
  );
  const yMax = 200;

  return (
    <div className="app">
      <h1>Couverture opérationnelle</h1>
      <div className="flex-aligned-center">
        <UiButton
          isLoading={isFetching}
          onClick={() => dispatch(actions.helloSapeurThunk())}
        >
          Hello sapeurs !
        </UiButton>
        <div>
          Message:{" "}
          {message ? <strong>{message}</strong> : "Pas encore de message..."}
        </div>
      </div>

      <div className="flex-aligned-center">
        <UiDatepicker
          value={eventStartDate}
          onChange={(e) => {
            setEventStartDate(e.target.value);
          }}
        />
        <UiButton
          isLoading={isReplayedEventBusPreparing}
          onClick={() =>
            dispatch(
              actions.setReplayedFromDateThunk(
                new Date(`${eventStartDate}T08:00`).toISOString(),
              ),
            )
          }
        >
          Préparer les données
        </UiButton>
        <UiButton
          onClick={() => dispatch(actions.initiateScheduledEventsThunk())}
        >
          Commencer
        </UiButton>
      </div>

      <p>
        Évenements :{" "}
        {areScheduledEventsPlaying ? "Ecoute en cours..." : "Arretés"}
      </p>
      <hr />
      <div style={{ display: "flex" }}>
        <div className="graph">
          <h4>Historique des véhicules disponibles</h4>
          <StreamingLineChart
            yMax={yMax}
            data={vehicleAvailableCounts}
            tSlice={15000}
            tUnit="hour"
            variableToStyle={lineVariableToStyle}
            tMax={
              vehicleAvailableCounts.length
                ? max(
                    vehicleAvailableCounts.map(
                      ({ timestamp }) => new Date(timestamp),
                    ),
                  )
                : undefined
            }
          />
        </div>
        <div className="graph">
          <h4>Disponibilité actuelle des véhicules</h4>
          <BarChart
            yMax={yMax}
            inputSamples={vehicleStatusInputSamples}
            countKindOptions={countKindOptions}
            roleOptions={roleOptions}
            barVariableToStyle={barVariableToStyle}
          />
        </div>
      </div>

      <pre>{JSON.stringify(vehicleAvailableCounts.slice(-5), null, 2)}</pre>
      {/* <WelcomeUi /> */}
    </div>
  );
};
