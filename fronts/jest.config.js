module.exports = {
  projects: [
    "<rootDir>/apps/enki",
    "<rootDir>/libs/calculator",
    "<rootDir>/apps/test-react",
    "<rootDir>/libs/core-couv-ops",
    "<rootDir>/libs/ui",
  ],
};
