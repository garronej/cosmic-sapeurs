==========
Repository
==========

- 1 Backend (shared) : ![backend documentation](/backend/README.rst)
- 3 Front
    * front commandment (dedicated to the firefighter on a daily basis: operational coverage management, simulator, decision support,...)
    * front passive watch (intended for mayors, elected officials, general): high-level watch, alerts)
    * front crisis orchestration : tasks management

