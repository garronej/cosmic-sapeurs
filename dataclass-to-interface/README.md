## Python side

### Install

```
cd ./dataclass-to-interface
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
pip install -e ../backend/src/
npm install
```

### Usage

```
cd ./dataclass-to-interface
./generate-interfaces.sh
```
