const { compileFromFile } = require("json-schema-to-typescript");
const fs = require("fs");
const path = require("path");

// const pathToFolderOfJsonSchema = process.argv[2];
const pathToFolderOfJsonSchema = `${__dirname}/temp-json-schemas`; // /temp-couvops-json-schemas`
const pathToFolderOfOutputInterfaces =  `${__dirname}/../../fronts/libs/interfaces/src/generated`;

const convert = () => {
  if (!pathToFolderOfJsonSchema)
    throw new Error("Please provide a folder where the json-schema stand");
  if (!pathToFolderOfOutputInterfaces)
    throw new Error("Please provide a folder where to store the interfaces");

  fs.rmdirSync(pathToFolderOfOutputInterfaces, { recursive: true }, (err) => {
    if (err) throw err;
    console.log(`${dir} is deleted!`);
  });
  fs.mkdirSync(pathToFolderOfOutputInterfaces)

  fs.readdir(pathToFolderOfJsonSchema, function (err, files) {
    if (err) return console.log("Unable to scan directory: " + err);

    files.forEach(function (file) {
      const pathToJsonSchemaFile = path.join(pathToFolderOfJsonSchema, file);
      const pathToInterfaceFile = path.join(
        pathToFolderOfOutputInterfaces,
        file.replace(".json", ".ts")
      );

      // actual convertion :
      compileFromFile(pathToJsonSchemaFile).then((ts) => {
        fs.writeFileSync(pathToInterfaceFile, ts);
      });
    });
  });
};

convert();

