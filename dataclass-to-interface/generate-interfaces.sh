#!/bin/bash
source venv/bin/activate
python src/dataclass_to_json_schema.py --input-python-dir "../backend/src/domain/couv_ops/data_transfert_objects" --output-json-dir "./src/temp-json-schemas"
node src/jsonSchemaToInterface.js
